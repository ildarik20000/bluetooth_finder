import UIKit
import AVKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    private lazy var controller = window.rootViewController as! FlutterViewController
    private lazy var ringChannel = FlutterMethodChannel(name: "ring", binaryMessenger: controller.binaryMessenger)
    
    private var player: AVAudioPlayer?
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        GeneratedPluginRegistrant.register(with: self)
        
        ringChannel.setMethodCallHandler { call, result in
            if call.method == "ring" {
                let session = AVAudioSession.sharedInstance()
                try? session.setCategory(.playAndRecord, mode: .default, options: [.allowBluetooth, .allowBluetoothA2DP])
                try? session.overrideOutputAudioPort(.none)
                try? session.setActive(true)
                let headphonesFound: Bool
                if let availableInputs = session.availableInputs {
                    headphonesFound = availableInputs.contains(where: { $0.portType == .bluetoothHFP })
                } else {
                    headphonesFound = false
                }
                if headphonesFound {
                    let path = Bundle.main.url(forResource: "ring_sound", withExtension: "mp3")!
                    self.player?.stop()
                    self.player = try? AVAudioPlayer(contentsOf: path)
                    self.player?.numberOfLoops = 3
                    self.player?.play()
                }
                
                result(headphonesFound)
            }
        }
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
}
