import 'package:bluetooth_finder/config/colors.dart';
import 'package:flutter/material.dart';

Widget headerBlock(Widget child, BuildContext context) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 15),
    height: 80,
    decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        border: Border(
            bottom: BorderSide(
                color: ColorsConfig.black.withOpacity(0.257), width: 0.5))),
    child: child,
  );
}
