import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BlockFAQ extends StatefulWidget {
  String title;
  String text;
  
  BlockFAQ(this.title, this.text);

  @override
  State<BlockFAQ> createState() => _BlockFAQState();
}

class _BlockFAQState extends State<BlockFAQ> with TickerProviderStateMixin {
  bool isOpen = false;
  double angle = 0.0;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 16,
        left: 24,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            hoverColor: Color.fromRGBO(0, 0, 0, 0),
            splashColor: Color.fromRGBO(0, 0, 0, 0),
            focusColor: Color.fromRGBO(0, 0, 0, 0),
            highlightColor: Color.fromRGBO(0, 0, 0, 0),
            onTap: () {
              setState(() {
                angle += 0.125;
                isOpen = !isOpen;
              });
            },
            child: Container(
              padding: EdgeInsets.only(right: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      widget.title,
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  AnimatedRotation(
                    turns: angle,
                    duration: Duration(milliseconds: 200),
                    child: Image.asset(
                      "assets/images/plus.png",
                      color: Theme.of(context).hoverColor,
                      width: 24,
                    ),
                  ),
                ],
              ),
            ),
          ),
          AnimatedSize(
            duration: Duration(milliseconds: 200),
            child: Container(
                padding: EdgeInsets.only(top: 16),
                constraints: BoxConstraints(
                    maxHeight: this.isOpen ? double.infinity : 0),
                child: Text(
                  widget.text,
                  style: GoogleFonts.manrope(
                      color: Theme.of(context).hoverColor,
                      fontWeight: FontWeight.w400,
                      fontSize: 15),
                )),
          ),
          SizedBox(
            height: 16,
          ),
          Divider(
            height: 2,
          ),
        ],
      ),
    );
  }
}
