import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:auto_size_text/auto_size_text.dart';

class MapDevices extends StatelessWidget {
  String name;
  String distance;
  String image;
  MapDevices(this.name, this.distance, this.image, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 34,
      child: Stack(children: [
        Container(
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 4,
                  blurRadius: 20,
                  offset: Offset(0, 20), // changes position of shadow
                ),
              ],
              borderRadius: BorderRadius.circular(8),
              color: Theme.of(context).focusColor),
          margin: EdgeInsets.only(bottom: 3),
          child: Container(
            padding: EdgeInsets.all(6),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 4,
                  ),
                  Image.asset(
                    "assets/images/${image}.png",
                    width: 20,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: AutoSizeText(
                      name + ", " + distance + "m",
                      maxLines: 1,
                      minFontSize: 2,
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13.sp,
                          color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              child: Image.asset(
                "assets/images/Rectangle.png",
                color: Theme.of(context).focusColor,
                width: 6,
              ),
            ))
      ]),
    );
  }
}
