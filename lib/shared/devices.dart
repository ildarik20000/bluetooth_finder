import 'package:auto_size_text/auto_size_text.dart';
import 'package:bluetooth_finder/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

import '../models/gadgets_model.dart';

class Devices extends StatelessWidget {
  Gadget gadget;
  Devices(this.gadget, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Theme.of(context).cardColor),
      padding: EdgeInsets.all(16),
      height: 88,
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.all(8),
            width: 56,
            height: 56,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Theme.of(context).hintColor),
            child: Image.asset(
              "assets/images/${gadget.type.name}.png",
              width: 40,
              color: Theme.of(context).hoverColor,
            ),
          ),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: AutoSizeText(gadget.name,
                            maxLines: 2,
                            style: GoogleFonts.manrope(
                                fontWeight: FontWeight.w600,
                                fontSize: 17,
                                color: Theme.of(context).hoverColor)),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Image.asset(
                        "assets/images/signal${gadget.signal}.png",
                        width: 24,
                        color: Theme.of(context).hoverColor,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    Text(gadget.distance + "m",
                        style: GoogleFonts.manrope(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Theme.of(context).hoverColor)),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class DevicesAvailable extends StatelessWidget {
  String text;
  BuildContext context;
  int signal;
  DevicesAvailable(this.text, this.context, this.signal, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Theme.of(context).cardTheme.color),
      padding: EdgeInsets.all(16),
      height: 88,
      child: Row(
        children: [
          Image.asset(
            "assets/images/map.png",
            width: 56,
          ),
          SizedBox(
            width: 16,
          ),
          Column(
            children: [
              Text(text,
                  style: GoogleFonts.manrope(
                      fontWeight: FontWeight.w600,
                      fontSize: 17,
                      color: Theme.of(context).hoverColor)),
              SizedBox(
                width: 16,
              ),
              Image.asset(
                "assets/images/signal${signal}.png",
                color: Theme.of(context).hoverColor,
                width: 24,
              )
            ],
          )
        ],
      ),
    );
  }
}
