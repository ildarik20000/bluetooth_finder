import 'package:bluetooth_finder/controllers/navigation_bar_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/find_gadgets_controller.dart';

class BlockFAQSpecial extends StatefulWidget {
  String title;
  String text;
  String text2;
  String text3;

  BlockFAQSpecial(this.title, this.text, this.text2, this.text3);

  @override
  State<BlockFAQSpecial> createState() => _BlockFAQSpecialState();
}

class _BlockFAQSpecialState extends State<BlockFAQSpecial>
    with TickerProviderStateMixin {
  bool isOpen = false;
  double angle = 0.0;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 16,
        left: 24,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            hoverColor: Color.fromRGBO(0, 0, 0, 0),
            splashColor: Color.fromRGBO(0, 0, 0, 0),
            focusColor: Color.fromRGBO(0, 0, 0, 0),
            highlightColor: Color.fromRGBO(0, 0, 0, 0),
            onTap: () {
              setState(() {
                angle += 0.125;
                isOpen = !isOpen;
              });
            },
            child: Container(
              padding: EdgeInsets.only(right: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      widget.title,
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  AnimatedRotation(
                    turns: angle,
                    duration: Duration(milliseconds: 200),
                    child: Image.asset(
                      "assets/images/plus.png",
                      color: Theme.of(context).hoverColor,
                      width: 24,
                    ),
                  ),
                ],
              ),
            ),
          ),
          AnimatedSize(
            duration: Duration(milliseconds: 200),
            child: Container(
                padding: EdgeInsets.only(top: 16),
                constraints: BoxConstraints(
                    maxHeight: this.isOpen ? double.infinity : 0),
                child: InkWell(
                  hoverColor: Color.fromRGBO(0, 0, 0, 0),
                  splashColor: Color.fromRGBO(0, 0, 0, 0),
                  focusColor: Color.fromRGBO(0, 0, 0, 0),
                  highlightColor: Color.fromRGBO(0, 0, 0, 0),
                  onTap: () {
                    final devicesController =
                        Get.put(NavigationBarController());

                    devicesController.settingsPage.value = 3;
                    devicesController.update();
                  },
                  child: RichText(
                      text: TextSpan(
                          text: widget.text,
                          style: GoogleFonts.manrope(
                              color: Theme.of(context).hoverColor,
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                          children: [
                        TextSpan(
                          text: widget.text2,
                          style: GoogleFonts.manrope(
                              color: Color.fromRGBO(0, 122, 255, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        ),
                        TextSpan(
                          text: widget.text3,
                          style: GoogleFonts.manrope(
                              color: Theme.of(context).hoverColor,
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )
                      ])),
                )),
          ),
          SizedBox(
            height: 16,
          ),
          Divider(
            height: 2,
          ),
        ],
      ),
    );
  }
}
