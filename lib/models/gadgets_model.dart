import '../controllers/find_gadgets_controller.dart';

class Gadget {
  String id;
  String name;
  DeviceType type;
  String distance;
  String battery;
  int signal;
  bool connect;
  Gadget(this.id, this.name, this.type, this.signal, this.distance, this.battery,
      this.connect);
}
