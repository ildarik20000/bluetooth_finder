import 'package:get/get.dart';

import '../models/gadgets_model.dart';

enum DeviceType {
  laptop,
  desktop,
  mobile,
  headphones,
  tablet,
  watch,
  tv,
  mouse,
  other
}

DeviceType getTypeDevice(String name) {
  DeviceType? deviceType;
  DeviceType.values.forEach(
    (e) {
      e.deviceNames.forEach((element) {
        if ((name.toLowerCase().contains(element.toLowerCase())) &&
            deviceType == null) {
          deviceType = e;
        }
      });
    },
  );
  return deviceType ?? DeviceType.other;
}

extension DeviceTypeExt on DeviceType {
  List<String> get deviceNames {
    switch (this) {
      case DeviceType.desktop:
        return ['PC', 'Computer', 'iMac', 'Компьютер', 'ПК', 'i mac'];
      case DeviceType.laptop:
        return ['MacBook', 'Laptop', 'Book pro', 'book air', 'mac book'];
      case DeviceType.headphones:
        return ['Наушник', 'Air pods', 'AirPods', 'Headphones'];
      case DeviceType.mobile:
        return ['phone', 'mobile', 'iphone', 'ios'];
      case DeviceType.tablet:
        return ['ipad', 'i pad', 'tablet'];
      case DeviceType.watch:
        return ['watch', 'Часы', 'band'];
      default:
        return ['Unknown'];
    }
  }
}

class FindGadgetsController extends GetxController {
  var tapIndexTypeGadgets = 0.obs;
  var tapStringTypeGadgets = "all".obs;
  List<Gadget> allGadgets = List<Gadget>.empty(growable: true).obs;
  List<Gadget> sortGadgets = List<Gadget>.empty(growable: true).obs;
  // var connectGadget = List<Gadget>.empty(growable: true).obs;
  // List<Gadget> avialableGadget = List<Gadget>.empty(growable: true).obs;

  var devicesTap = "123".obs;

  int getSignal(int rssi) {
    if (rssi > -65) return 4;
    if (rssi > -75) return 3;
    if (rssi > -85) return 2;
    return 1;
  }

  var devicesTapDistance = "0".obs;
  var devicesTapName = "".obs;

  var devicesTapType = false.obs;
  getDistanceSelectDevices() {
    for (int i = 0; i < allGadgets.length; i++) {
      if (allGadgets[i].id == devicesTap.value) {
        print(allGadgets[i].type);
        if (allGadgets[i].type.name == "headphones") {
          devicesTapType.value = true;
          update();
        } else {
          devicesTapType.value = false;
          update();
        }
        devicesTapDistance.value = allGadgets[i].distance;

        try {
          devicesTapDistance.value =
              double.parse(devicesTapDistance.value) > 100
                  ? "100"
                  : devicesTapDistance.value;
        } catch (_) {
          devicesTapDistance.value = "0";
        }
        return;
      }
    }
    //devicesTapDistance.value = "100";
    sortGadgets = allGadgets;
    sortGadgets.sort((a, b) => a.distance.compareTo(b.distance));
    update();
  }

  setAllGadget(List<Gadget> gadgets) {
    bool available = false;
    for (int i = 0; i < gadgets.length; i++) {
      for (int k = 0; k < allGadgets.length; k++) {
        if (gadgets[i].id == allGadgets[k].id) {
          available = true;

          break;
        }
      }
      if (!available) {
        print("add");
        allGadgets.add(gadgets[i]);
      }
    }

    for (int i = 0; i < allGadgets.length; i++) {
      bool found = false;
      for (int k = 0; k < gadgets.length; k++) {
        if (allGadgets[i].id == gadgets[k].id) {
          found = true;
          allGadgets[i] = gadgets[k];
          break;
        }
      }
      if (!found) {
        print("delete");
        allGadgets.removeAt(i);
      }
    }
    //connectGadget.value = gadgets;
    //allGadgets.sort((a, b) => a.distance.compareTo(b.distance));
    update();
  }

  // setAvialableGadget(List<Gadget> gadgets) {
  //   allGadgets = gadgets;
  // }
}
