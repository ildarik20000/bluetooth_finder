import 'package:get/get.dart';

class OnboardingController extends GetxController {
  var tapIndex = 0.obs;
  var animation = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    startAnimation();
  }

  startAnimation() async {
    animation.value = true;
    await Future.delayed(Duration(milliseconds: 1300));
    animation.value = false;
    backAnimation();
  }

  backAnimation() async {
    animation.value = false;
    await Future.delayed(Duration(milliseconds: 1300));
    animation.value = true;
    startAnimation();
  }
}
