import 'package:bluetooth_finder/pages/devices_page.dart';
import 'package:bluetooth_finder/pages/main_page.dart';
import 'package:bluetooth_finder/pages/map_page.dart';
import 'package:bluetooth_finder/pages/settings_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PagesController extends GetxController {
  var indexPageSettings = 0.obs;
  var devicesPageSettings = 0.obs;

  Widget getSettingsPage() {
    return SettingsPage();
  }

  Widget getDevicesPage() {
    if (devicesPageSettings.value == 0) return DevicesPage();
    if (devicesPageSettings.value == 1) return MapPage();
    return DevicesPage();
  }

  setMapPage() {
    devicesPageSettings.value = 1;
    update();
  }

  setDevicesPage() {
    devicesPageSettings.value = 0;
    update();
  }
}
