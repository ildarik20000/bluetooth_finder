import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:get/get.dart' hide Trans;

class NavigationBarController extends GetxController {
  var tabIndex = 1.obs;

  var devicesPage = 0.obs;
  var settingsPage = 0.obs;
  var articlesPage = 0.obs;

  List<String> articlesTitle = [
    "What is a Bluetooth tracker?",
    "Bluetooth Trackers: How It Works",
    "5 Things To Check When Buying A Bluetooth Tracker",
    "If your iPhone, iPad, or iPod touch is lost or stolen",
    "Enjoy the Top 10 Benefits of Bluetooth",
    "Is Bluetooth Dangerous For Your Brain? Safety & Risks",
    "Healthy headphone use: How loud and how long?",
    "8 Ways You're Using Your Headphones Wrong",
    "How to keep track of what matters",
    "If your Apple Watch is lost or stolen",
    "Find your lost AirPods",
    "Set up AirPods with your Mac and other Bluetooth devices",
    "5 Easy Tips to Extend the Life of Your Headphones",
    "How to Clean AirPods, Earbuds, and Headphones",
  ];

  final themeController = Get.put(ThemeDataController());

  // int get tapMenuIcon => devicesPage.value;
  int get pageIndex => tabIndex.value;

  void changeTabIndex(int index) {
    if (index == 0) {
      // final onboardingController = Get.put(OnboardingController());

      tabIndex.value = index;
    }
    if (index == 1) {
      tabIndex.value = index;
    }
    if (index == 2) {
      tabIndex.value = index;
    }
    update();
  }
}
