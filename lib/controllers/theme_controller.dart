import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

class ThemeDataController extends GetxController {
  var typeTheme = 0.obs;

  getTheme() async{
    print("GET THEME");
    typeTheme.value = Hive.box('settings').get('theme', defaultValue: 0);
    await setTheme(typeTheme.value);
  }

  setTheme(int typeTheme) {
    Hive.box('settings').put('theme', typeTheme);
    if (typeTheme == 0) {
      setClassicTheme();
    }
    if (typeTheme == 1) {
      setRedTheme();
    }
    if (typeTheme == 2) {
      setGreenTheme();
    }
    if (typeTheme == 3) {
      setDark1Theme();
    }

    if (typeTheme == 4) {
      setDark2Theme();
    }
  }

  setClassicTheme() {
    typeTheme.value = 0;
    Get.changeTheme(themeClassic());
  }

  setRedTheme() {
    typeTheme.value = 1;
    Get.changeTheme(themeRed());
  }

  setGreenTheme() {
    typeTheme.value = 2;
    Get.changeTheme(themeGreen());
  }

  setDark1Theme() {
    typeTheme.value = 3;
    Get.changeTheme(themeDark1());
  }

  setDark2Theme() {
    typeTheme.value = 3;
    Get.changeTheme(themeDark2());
  }

  ThemeData themeGreen() {
    return ThemeData(
        cardTheme: CardTheme(color: Color.fromRGBO(246, 246, 246, 1)),
        focusColor: Color.fromRGBO(23, 168, 99, 1),
        hoverColor: Colors.black,
        hintColor: Colors.white,
        cardColor: Color.fromRGBO(226, 249, 238, 1),
        indicatorColor: Color.fromRGBO(40, 80, 0, 1),
        splashColor: Color.fromRGBO(70, 216, 146, 1),
        scaffoldBackgroundColor: Color.fromRGBO(255, 255, 255, 1));
  }

  ThemeData themeClassic() {
    return ThemeData(
        cardTheme: CardTheme(color: Color.fromRGBO(246, 246, 246, 1)),
        focusColor: Color.fromRGBO(0, 122, 255, 1),
        hoverColor: Colors.black,
        hintColor: Colors.white,
        cardColor: Color.fromRGBO(237, 246, 255, 1),
        indicatorColor: Color.fromRGBO(0, 102, 214, 1),
        splashColor: Color.fromRGBO(80, 167, 255, 1),
        scaffoldBackgroundColor: Color.fromRGBO(255, 255, 255, 1));
  }

  ThemeData themeRed() {
    return ThemeData(
        cardTheme: CardTheme(color: Color.fromRGBO(246, 246, 246, 1)),
        focusColor: Color.fromRGBO(228, 50, 50, 1),
        hoverColor: Colors.black,
        hintColor: Colors.white,
        cardColor: Color.fromRGBO(255, 237, 237, 1),
        indicatorColor: Color.fromRGBO(80, 0, 0, 1),
        splashColor: Color.fromRGBO(255, 173, 173, 1),
        scaffoldBackgroundColor: Color.fromRGBO(255, 255, 255, 1));
  }

  ThemeData themeDark1() {
    return ThemeData(
        cardTheme: CardTheme(color: Color.fromRGBO(54, 54, 54, 1)),
        hoverColor: Colors.white,
        hintColor: Color.fromRGBO(0, 122, 255, 1),
        focusColor: Color.fromRGBO(0, 122, 255, 1),
        cardColor: Color.fromRGBO(
          42,
          50,
          62,
          1,
        ),
        indicatorColor: Color.fromRGBO(0, 102, 214, 1),
        splashColor: Color.fromRGBO(80, 167, 255, 1),
        scaffoldBackgroundColor: Color.fromRGBO(28, 28, 30, 1));
  }

  ThemeData themeDark2() {
    return ThemeData(
        cardTheme: CardTheme(color: Color.fromRGBO(54, 54, 54, 1)),
        hoverColor: Colors.white,
        focusColor: Color.fromRGBO(191, 90, 242, 1),
        hintColor: Color.fromRGBO(191, 90, 242, 1),
        cardColor: Color.fromRGBO(
          69,
          47,
          81,
          1,
        ),
        indicatorColor: Color.fromRGBO(148, 52, 197, 1),
        splashColor: Color.fromRGBO(148, 52, 197, 1),
        scaffoldBackgroundColor: Color.fromRGBO(28, 28, 30, 1));
  }

  getButtonColor() {
    if (typeTheme == 0) {
      Color.fromRGBO(0, 122, 255, 1);
    }
    if (typeTheme == 1) {
      Color.fromRGBO(228, 50, 50, 1);
    }
  }
}
