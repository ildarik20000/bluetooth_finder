import 'package:apphud/apphud.dart';
import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:bluetooth_finder/pages/main_page.dart';
import 'package:bluetooth_finder/pages/onboarding_page.dart';
import 'package:bluetooth_finder/pages/subscription_page.dart';
import 'package:bluetooth_finder/services/find_bluetooth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

bool subscribe = false;

bool init = false;

final appID = '1588554686';

final to =
    '';
final pp =
    '';
final support = '';

final appHudKey = '';
void main() async {
  await Hive.initFlutter();
  await Hive.openBox('settings');
  await ThemeDataController().getTheme();
  await Apphud.start(apiKey: appHudKey);
  subscribe = await Apphud.hasActiveSubscription();
  WidgetsFlutterBinding.ensureInitialized();
  MobileAds.instance.initialize();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      //theme: getTheme,
      home: ScreenUtilInit(
          designSize: const Size(375, 844), builder: () => HomePage()),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return !Hive.box('settings').get('onboardingShow', defaultValue: false)
        ? OnboardingPage()
        : subscribe
            ? MainPage()
            : SubscriptionPage(
                fromOnboarding: true,
              ); //MainPage();
  }
}
