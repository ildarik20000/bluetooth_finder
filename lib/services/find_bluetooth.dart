import 'dart:async';
import 'dart:math';

import 'package:bluetooth_finder/models/gadgets_model.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:get/get.dart';

import '../controllers/find_gadgets_controller.dart';
import '../main.dart';

class FindBluetooth {
  final gadgetsController = Get.put(FindGadgetsController());
  bool isBluetoothTurnedOn = false;
  StreamSubscription? bluetoothSubscription;
  StreamSubscription? devicesSubscription;

  initBluetooth() {
    init=true;
    List<Gadget> scanResults = [];
    List<Gadget> connectScanResults = [];
    List<Gadget> allResult = [];
    bluetoothSubscription =
        Stream.periodic(Duration(seconds: 1)).listen((_) async {
      isBluetoothTurnedOn = await FlutterBlue.instance.isOn;
    });

    devicesSubscription =
        Stream.periodic(Duration(seconds: 3)).listen((_) async {
      if (isBluetoothTurnedOn) {
        final Stream<ScanResult> devices =
            FlutterBlue.instance.scan(scanMode: ScanMode.lowLatency);

        scanResults.clear();
        connectScanResults.clear();
        allResult.clear();
        final devicesSubscription = devices.listen((result) {
          if (result.device.name != "") {
            gadgetsController.getDistanceSelectDevices();
            Gadget gadget = Gadget(
                result.device.id.toString(),
                result.device.name,
                getTypeDevice(result.device.name),
                gadgetsController.getSignal(result.rssi),
                pow(10, (-60 - result.rssi) / (10 * 2)).floor().toString(),
                "battery",
                result.advertisementData.connectable);
            allResult.add(gadget);
            // if (result.advertisementData.connectable) {

            //   print('Result name: ' +
            //       result.device.name +
            //       ' connect: ' +
            //       result.advertisementData.connectable.toString());
            //   connectScanResults.add(gadget);

            //   print(connectScanResults.length);
            // } else {
            //   scanResults.add(gadget);
            // }
          }
        });

        // devices.map((event) async {
        //    final Stream<ScanResult> devices =
        //     event.state;
        //   List<BluetoothService> services = await event.discoverServices();
        //   services.forEach((service) {
        //     // do something with service
        //   });
        //   var characteristics = service.characteristics;
        //   for (BluetoothCharacteristic c in characteristics) {
        //     List<int> value = await c.read();
        //     print(value);
        //   }

        //   await c.write([0x12, 0x34]);
        // });

        scanResults.sort((a, b) => a.distance.compareTo(b.distance));
        connectScanResults.sort((a, b) => a.distance.compareTo(b.distance));
        allResult.sort((a, b) => a.distance.compareTo(b.distance));

        await Future.delayed(Duration(seconds: 2), () {
          FlutterBlue.instance.stopScan();
          devicesSubscription.cancel();
        });
        await updateState(scanResults, connectScanResults, allResult);
      }
    });
  }

  updateState(List<Gadget> scan1, List<Gadget> scan2, List<Gadget> scan3) {
    gadgetsController.setAllGadget(scan3);
    print("update");
    // gadgetsController.allGadgets = scan3;
    // gadgetsController.avialableGadget = scan2;
    gadgetsController.update();
  }
}
