class AdHelper {
  static String get bannerAdUnitId {
    return 'ca-app-pub-3940256099942544/2934735716';
  }

  static String get interstitialAdUnitId {
    return "ca-app-pub-9765019211152110/6994339488";
  }

  static String get rewardedAdUnitId {
    return "ca-app-pub-9765019211152110/6994339488";
  }
}
