import 'package:bluetooth_finder/config/colors.dart';
import 'package:bluetooth_finder/config/text.dart';
import 'package:bluetooth_finder/controllers/navigation_bar_controller.dart';
import 'package:bluetooth_finder/main.dart';
import 'package:bluetooth_finder/models/gadgets_model.dart';
import 'package:bluetooth_finder/pages/map_page.dart';
import 'package:bluetooth_finder/pages/subscription_page.dart';
import 'package:bluetooth_finder/services/ad_helper.dart';
import 'package:bluetooth_finder/services/find_bluetooth.dart';
import 'package:bluetooth_finder/shared/devices.dart';
import 'package:bluetooth_finder/shared/header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:hive/hive.dart';

import '../controllers/find_gadgets_controller.dart';

class DevicesPage extends StatefulWidget {
  @override
  State<DevicesPage> createState() => _DevicesPageState();
}

class _DevicesPageState extends State<DevicesPage> {
  final gadgetsController = Get.put(FindGadgetsController());

  final bottomBarController = Get.put(NavigationBarController());

  late BannerAd _bannerAd;

  bool _isBannerAdReady = false;

  @override
  void initState() {
    _bannerAd = BannerAd(
      adUnitId: AdHelper.bannerAdUnitId,
      request: AdRequest(),
      size: AdSize.banner,
      listener: BannerAdListener(
        onAdLoaded: (_) {
          setState(() {
            _isBannerAdReady = true;
          });
        },
        onAdFailedToLoad: (ad, err) {
          print('Failed to load a banner ad: ${err.message}');
          _isBannerAdReady = false;
          ad.dispose();
        },
      ),
    );

    _bannerAd.load();
  }

  @override
  void dispose() {
    // TODO: Dispose a BannerAd object
    _bannerAd.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        headerBlock(
            Stack(
              children: [
                Container(),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    "Find Gadgets",
                    style: GoogleFonts.manrope(
                        fontWeight: FontWeight.w600,
                        fontSize: 22,
                        color: Theme.of(context).hoverColor),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    onTap: () {
                      print(bottomBarController.devicesPage.value);
                      bottomBarController.devicesPage.value = 1;
                      bottomBarController.update();
                    },
                    child: Container(
                      child: Image.asset(
                        "assets/images/map.png",
                        color: Theme.of(context).hoverColor,
                        width: 24,
                      ),
                    ),
                  ),
                )
              ],
            ),
            context),
        Obx(() => Container(
              height: 72,
              padding: EdgeInsets.only(left: 6),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    typeGadgets(TextConfig.all, context,
                        gadgetsController.tapIndexTypeGadgets.value == 0, () {
                      gadgetsController.tapIndexTypeGadgets.value = 0;
                      gadgetsController.tapStringTypeGadgets.value = "all";
                    }),
                    typeGadgets(TextConfig.pc, context,
                        gadgetsController.tapIndexTypeGadgets.value == 1, () {
                      gadgetsController.tapIndexTypeGadgets.value = 1;
                      gadgetsController.tapStringTypeGadgets.value = "desktop";
                    }),
                    typeGadgets(TextConfig.laptop, context,
                        gadgetsController.tapIndexTypeGadgets.value == 2, () {
                      gadgetsController.tapIndexTypeGadgets.value = 2;
                      gadgetsController.tapStringTypeGadgets.value = "laptop";
                    }),
                    typeGadgets(TextConfig.tablet, context,
                        gadgetsController.tapIndexTypeGadgets.value == 3, () {
                      gadgetsController.tapIndexTypeGadgets.value = 3;
                      gadgetsController.tapStringTypeGadgets.value = "tablet";
                    }),
                    typeGadgets(TextConfig.speaker, context,
                        gadgetsController.tapIndexTypeGadgets.value == 4, () {
                      gadgetsController.tapIndexTypeGadgets.value = 4;
                      gadgetsController.tapStringTypeGadgets.value = "speaker";
                    }),
                    typeGadgets(TextConfig.mouse, context,
                        gadgetsController.tapIndexTypeGadgets.value == 5, () {
                      gadgetsController.tapIndexTypeGadgets.value = 5;
                      gadgetsController.tapStringTypeGadgets.value = "mouse";
                    }),
                    typeGadgets(TextConfig.headphones, context,
                        gadgetsController.tapIndexTypeGadgets.value == 6, () {
                      gadgetsController.tapIndexTypeGadgets.value = 6;
                      gadgetsController.tapStringTypeGadgets.value =
                          "headphones";
                    }),
                    typeGadgets(TextConfig.smartphone, context,
                        gadgetsController.tapIndexTypeGadgets.value == 7, () {
                      gadgetsController.tapIndexTypeGadgets.value = 7;
                      gadgetsController.tapStringTypeGadgets.value =
                          "smartphone";
                    }),
                    typeGadgets(TextConfig.watch, context,
                        gadgetsController.tapIndexTypeGadgets.value == 8, () {
                      gadgetsController.tapIndexTypeGadgets.value = 8;
                      gadgetsController.tapStringTypeGadgets.value = "watch";
                    }),
                    typeGadgets(TextConfig.other, context,
                        gadgetsController.tapIndexTypeGadgets.value == 9, () {
                      gadgetsController.tapIndexTypeGadgets.value = 9;
                      gadgetsController.tapStringTypeGadgets.value = "other";
                    }),
                    Container(
                      width: 16,
                    ),
                  ],
                ),
              ),
            )),
        SizedBox(
          height: 16.h,
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  TextConfig.connectDevices,
                  style: GoogleFonts.manrope(
                      fontWeight: FontWeight.w500,
                      fontSize: 17,
                      color: Theme.of(context).hoverColor),
                ),
                SizedBox(
                  height: 16,
                ),
                Expanded(
                  //height: 232,
                  child: GetBuilder<FindGadgetsController>(builder: (_) {
                    return gadgetsController.allGadgets.isNotEmpty
                        ? ListView.builder(
                            itemCount: gadgetsController.allGadgets.length,
                            itemBuilder: (context, index) {
                              return gadgetsController
                                          .tapStringTypeGadgets.value ==
                                      "all"
                                  ? InkWell(
                                      onTap: () {
                                        if (subscribe ||
                                            Hive.box('settings').get(
                                                    'tapDevice',
                                                    defaultValue: 0) <
                                                3) {
                                          Hive.box('settings').put(
                                              'tapDevice',
                                              Hive.box('settings').get(
                                                      'tapDevice',
                                                      defaultValue: 0) +
                                                  1);
                                          final devicesController =
                                              Get.put(FindGadgetsController());
                                          devicesController.devicesTap.value =
                                              gadgetsController
                                                  .allGadgets[index].id;
                                          gadgetsController
                                                  .devicesTapName.value =
                                              gadgetsController
                                                  .sortGadgets[index].name;
                                          bottomBarController
                                              .devicesPage.value = 2;
                                          bottomBarController.update();
                                          devicesController.update();
                                        } else {
                                          Get.to(() => SubscriptionPage(
                                                fromDevice: true,
                                              ));
                                        }
                                      },
                                      child: Devices(
                                        gadgetsController.allGadgets[index],
                                      ),
                                    )
                                  : gadgetsController.tapStringTypeGadgets.value
                                              .toLowerCase() ==
                                          gadgetsController
                                              .allGadgets[index].type.name
                                              .toLowerCase()
                                      ? InkWell(
                                          onTap: () {
                                            if (subscribe ||
                                                Hive.box('settings').get(
                                                        'tapDevice',
                                                        defaultValue: 0) <
                                                    3) {
                                              Hive.box('settings').put(
                                                  'tapDevice',
                                                  Hive.box('settings').get(
                                                          'tapDevice',
                                                          defaultValue: 0) +
                                                      1);
                                              final devicesController = Get.put(
                                                  FindGadgetsController());
                                              devicesController
                                                      .devicesTap.value =
                                                  gadgetsController
                                                      .allGadgets[index].id;
                                              bottomBarController
                                                  .devicesPage.value = 2;
                                              bottomBarController.update();
                                              devicesController.update();
                                            } else {
                                              Get.to(() => SubscriptionPage(
                                                    fromDevice: true,
                                                  ));
                                            }
                                          },
                                          child: Devices(
                                            gadgetsController.allGadgets[index],
                                          ))
                                      : Container();
                            })
                        : Center(
                            child: Text(
                            "Not found gadget",
                            style: GoogleFonts.manrope(
                              fontWeight: FontWeight.w400,
                              fontSize: 17,
                              color: Theme.of(context).hoverColor,
                            ),
                          ));
                  }),
                ),
                SizedBox(
                  height: 16.h,
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: GestureDetector(
                    onTap: () {
                      bottomBarController.devicesPage.value = 3;
                      bottomBarController.update();
                    },
                    child: Container(
                      alignment: Alignment.bottomLeft,
                      //height: 24.h,
                      child: Text.rich(
                        TextSpan(
                          text: "Device not found? ",
                          children: [
                            TextSpan(
                                text: " Support",
                                style: TextStyle(
                                    color: Theme.of(context).hoverColor,
                                    fontSize: 17.sp,
                                    fontWeight: FontWeight.w400))
                          ],
                          style: GoogleFonts.manrope(
                            fontWeight: FontWeight.w400,
                            fontSize: 15.sp,
                            color:
                                Theme.of(context).hoverColor.withOpacity(0.6),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                if (_isBannerAdReady)
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: _bannerAd.size.width.toDouble(),
                      height: _bannerAd.size.height.toDouble(),
                      child: AdWidget(ad: _bannerAd),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget typeGadgets(
      String text, BuildContext context, bool disabled, VoidCallback onTap) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.only(left: 10),
        height: 32,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
                color: disabled
                    ? Theme.of(context).focusColor
                    : Theme.of(context).hoverColor)),
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
        child: Center(
          child: Text(text,
              style: GoogleFonts.manrope(
                  color: Theme.of(context).hoverColor,
                  fontWeight: FontWeight.w400,
                  fontSize: 13)),
        ),
      ),
    );
  }
}
