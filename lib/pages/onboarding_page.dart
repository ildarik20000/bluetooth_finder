import 'package:app_review/app_review.dart';
import 'package:apphud/apphud.dart';
import 'package:bluetooth_finder/controllers/onboarding_controller.dart';
import 'package:bluetooth_finder/main.dart';
import 'package:bluetooth_finder/pages/main_page.dart';
import 'package:bluetooth_finder/pages/subscription_page.dart';
import 'package:bluetooth_finder/services/browser_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';

class OnboardingPage extends StatelessWidget {
  OnboardingPage({Key? key}) : super(key: key);
  var onboardController = Get.put(OnboardingController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(() {
        var typeOnboard = onboardController.tapIndex.value;
        return Container(
          child: Stack(
            children: [
              Image.asset(
                "assets/images/onboard${typeOnboard}.png",
                width: MediaQuery.of(context).size.height,
                fit: BoxFit.fill,
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.symmetric(horizontal: 24.w),
                  height: MediaQuery.of(context).size.height / 2 + 30,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 50.h,
                      ),
                      Text(
                        typeOnboard == 0
                            ? "Find\nbluetooth\ndevices"
                            : typeOnboard == 1
                                ? "Select the\ndevice in\nthe list"
                                : typeOnboard == 2
                                    ? "Use the ring\nto find the\ndevice"
                                    : typeOnboard == 3
                                        ? "Help us to\nimprove\nthe app"
                                        : "Use the map\nto find your\ndevices",
                        style: GoogleFonts.manrope(
                            fontWeight: FontWeight.w700,
                            fontSize: 48.sp,
                            height: 1,
                            color: Colors.black),
                      ),
                      SizedBox(
                        height: 24.h,
                      ),
                      Container(
                        // height: 50.h,
                        child: Text(
                          typeOnboard == 0
                              ? "Our aim is to save your time on\nlooking for your devices"
                              : typeOnboard == 1
                                  ? "The application will indicate the distance to the devicet"
                                  : typeOnboard == 2
                                      ? "So you will find the device faster and easier"
                                      : typeOnboard == 3
                                          ? "We constantly monitor reviews to make the app better"
                                          : "See where devices are located to find them",
                          style: GoogleFonts.manrope(
                              fontWeight: FontWeight.w400,
                              fontSize: 17.sp,
                              color: Colors.black),
                        ),
                      ),
                      SizedBox(
                        height: 24.h,
                      ),
                      Obx(() {
                        return Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            width: 100.h,
                            child: Stack(
                              children: [
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: AnimatedOpacity(
                                    opacity: onboardController.animation.value
                                        ? 0
                                        : 1,
                                    duration: Duration(
                                        milliseconds:
                                            onboardController.animation.value
                                                ? 200
                                                : 800),
                                    child: AnimatedContainer(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(40),
                                        color: Color.fromRGBO(0, 122, 255, 0.2),
                                      ),
                                      height: 56.h,
                                      width: onboardController.animation.value
                                          ? 40.h
                                          : 100.h,
                                      duration: Duration(
                                          milliseconds:
                                              onboardController.animation.value
                                                  ? 200
                                                  : 1000),
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    //onboardController.startAnimation();
                                    if (onboardController.tapIndex.value == 2)
                                      AppReview.requestReview;
                                    if (onboardController.tapIndex.value == 4) {
                                      Hive.box('settings')
                                          .put('onboardingShow', true);
                                      Get.offAll(SubscriptionPage(
                                        fromOnboarding: true,
                                      ));
                                    } else
                                      onboardController.tapIndex.value++;
                                  },
                                  child: AnimatedAlign(
                                    duration: Duration(
                                        milliseconds:
                                            onboardController.animation.value
                                                ? 200
                                                : 1000),
                                    alignment: onboardController.animation.value
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                    child: Container(
                                        //margin: EdgeInsets.only(right: 40),
                                        decoration: BoxDecoration(
                                            color:
                                                Color.fromRGBO(0, 122, 255, 1),
                                            borderRadius:
                                                BorderRadius.circular(50)),
                                        height: 56.h,
                                        width: 56.h,
                                        child: Center(
                                            child: Image.asset(
                                          "assets/images/chevron-right.png",
                                          width: 34.h,
                                        ))),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                      SizedBox(
                        height: 18.h,
                      ),
                      Expanded(
                        child: Container(
                            padding: EdgeInsets.only(
                                left: 15.w,
                                right: 15.w,
                                bottom: MediaQuery.of(context).size.height > 600
                                    ? 30.h
                                    : 10.h),
                            height: 24.h,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  onTap: termsOfUsePressed,
                                  child: Text(
                                    "Terms of Use",
                                    style: GoogleFonts.manrope(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12.sp,
                                        color:
                                            Color.fromRGBO(118, 118, 118, 1)),
                                  ),
                                ),
                                InkWell(
                                  onTap: () async {
                                    final result =
                                        await Apphud.restorePurchases();
                                    if (result.subscriptions
                                        .map((e) => e.isActive)
                                        .contains(true)) {
                                      Hive.box('settings')
                                          .put('onboardingShow', true);
                                      subscribe = true;
                                      Get.offAll(MainPage());
                                    }
                                  },
                                  child: Text(
                                    "Restore",
                                    style: GoogleFonts.manrope(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12.sp,
                                        color:
                                            Color.fromRGBO(118, 118, 118, 1)),
                                  ),
                                ),
                                InkWell(
                                  onTap: privacyPolicyPressed,
                                  child: Text(
                                    "Privacy Policy",
                                    style: GoogleFonts.manrope(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12.sp,
                                        color:
                                            Color.fromRGBO(118, 118, 118, 1)),
                                  ),
                                ),
                              ],
                            )),
                      )
                    ],
                  ),
                ),
              ),
              onboardController.tapIndex.value == 3
                  ? Center(
                      child: Container(
                        padding: EdgeInsets.only(bottom: 128),
                        child: Image.asset(
                          "assets/images/rate.png",
                          width: 328.w,
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        );
      }),
    );
  }
}
