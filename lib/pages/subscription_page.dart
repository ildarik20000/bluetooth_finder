import 'package:apphud/apphud.dart';
import 'package:apphud/models/apphud_models/apphud_paywall.dart';
import 'package:apphud/models/apphud_models/apphud_product.dart';
import 'package:bluetooth_finder/controllers/onboarding_controller.dart';
import 'package:bluetooth_finder/main.dart';
import 'package:bluetooth_finder/pages/devices_page.dart';
import 'package:bluetooth_finder/pages/main_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../services/browser_services.dart';

class SubscriptionPage extends StatelessWidget {
  bool fromOnboarding;
  bool fromDevice;
  SubscriptionPage(
      {this.fromOnboarding = false, this.fromDevice = false, Key? key})
      : super(key: key);
  var onboardController = Get.put(OnboardingController());
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 844),
        builder: () => Scaffold(
              backgroundColor: Color.fromRGBO(245, 245, 245, 1),
              body: Stack(
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 30.h),
                      child: Image.asset(
                        "assets/images/subpage.png",
                        width: 412.w,
                        fit: BoxFit.fill,
                      )),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.symmetric(horizontal: 20.w),
                      height: MediaQuery.of(context).size.height / 2 + 30,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(50),
                              topRight: Radius.circular(50))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 50.h,
                          ),
                          Text(
                            "Your Bluetooth\ngadgets\ndetector",
                            style: GoogleFonts.manrope(
                                fontWeight: FontWeight.w700,
                                fontSize: 46.sp,
                                height: 1,
                                color: Colors.black),
                          ),
                          SizedBox(
                            height: 14.h,
                          ),
                          Text(
                            "Unlimited use, ring feature and colorful themes, just \$9.99/week",
                            style: GoogleFonts.manrope(
                                fontWeight: FontWeight.w400,
                                fontSize: 17.sp,
                                color: Colors.black),
                          ),
                          SizedBox(
                            height: 14.h,
                          ),
                          Container(
                            height: 82.h,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "Recurring billing. Cancel anytime. Payment will be charged to your iTunes account at confirmation of purchase.",
                                          style: GoogleFonts.manrope(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 10.sp,
                                              color: Color.fromRGBO(
                                                  54, 54, 54, 1)),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 6.h,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          if (fromDevice) {
                                            Get.back();
                                          } else {
                                            if (fromOnboarding)
                                              Get.offAll(MainPage());
                                            else {
                                              Get.back();
                                            }
                                          }
                                        },
                                        child: Text(
                                          "Or continue with limited version",
                                          style: GoogleFonts.manrope(
                                              fontWeight: FontWeight.w400,
                                              decoration:
                                                  TextDecoration.underline,
                                              fontSize: 13.sp,
                                              color: Color.fromRGBO(
                                                  0, 122, 255, 1)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Obx(() {
                                  return Align(
                                    alignment: Alignment.bottomRight,
                                    child: Container(
                                      width: 100.h,
                                      child: Stack(
                                        children: [
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: AnimatedOpacity(
                                              opacity: onboardController
                                                      .animation.value
                                                  ? 0
                                                  : 1,
                                              duration: Duration(
                                                  milliseconds:
                                                      onboardController
                                                              .animation.value
                                                          ? 200
                                                          : 800),
                                              child: AnimatedContainer(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(40),
                                                  color: Color.fromRGBO(
                                                      0, 122, 255, 0.2),
                                                ),
                                                height: 56.h,
                                                width: onboardController
                                                        .animation.value
                                                    ? 40.h
                                                    : 100.h,
                                                duration: Duration(
                                                    milliseconds:
                                                        onboardController
                                                                .animation.value
                                                            ? 200
                                                            : 1000),
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () async {
                                              //onboardController.startAnimation();
                                              // subscribe = true;
                                              final paywalls = await Apphud
                                                  .paywallsDidLoadCallback();
                                              for (ApphudPaywall pw
                                                  in paywalls.paywalls) {
                                                for (ApphudProduct product
                                                    in pw.products!) {
                                                  if (product.productId ==
                                                      "lost_bluetooth_device_finder_week_sub_new") {
                                                    final res =
                                                        await Apphud.purchase(
                                                            product: product);
                                                    if (res.subscription!
                                                        .isActive) {
                                                      if (fromDevice)
                                                        Get.back();
                                                      else {
                                                        if (fromOnboarding)
                                                          Get.offAll(
                                                              MainPage());
                                                        else {
                                                          Get.back();
                                                        }
                                                      }
                                                      subscribe = true;
                                                    }
                                                  }
                                                }
                                              }
                                            },
                                            child: AnimatedAlign(
                                              duration: Duration(
                                                  milliseconds:
                                                      onboardController
                                                              .animation.value
                                                          ? 200
                                                          : 1000),
                                              alignment: onboardController
                                                      .animation.value
                                                  ? Alignment.centerLeft
                                                  : Alignment.centerRight,
                                              child: Container(
                                                  //margin: EdgeInsets.only(right: 40),
                                                  decoration: BoxDecoration(
                                                      color: Color.fromRGBO(
                                                          0, 122, 255, 1),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50)),
                                                  height: 56.h,
                                                  width: 56.h,
                                                  child: Center(
                                                      child: Image.asset(
                                                    "assets/images/chevron-right.png",
                                                    width: 34.h,
                                                  ))),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          Expanded(
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 15.w,
                                    right: 15.w,
                                    bottom:
                                        MediaQuery.of(context).size.height > 600
                                            ? 40.h
                                            : 10.h),
                                height: 24.h,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    InkWell(
                                      onTap: termsOfUsePressed,
                                      child: Text(
                                        "Terms of Use",
                                        style: GoogleFonts.manrope(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 12.sp,
                                            color: Color.fromRGBO(
                                                118, 118, 118, 1)),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () async {
                                        final result =
                                            await Apphud.restorePurchases();
                                        if (result.subscriptions
                                            .map((e) => e.isActive)
                                            .contains(true)) {
                                          subscribe = true;

                                          if (fromDevice)
                                            Get.back();
                                          else {
                                            if (fromOnboarding)
                                              Get.offAll(MainPage());
                                            else {
                                              Get.back();
                                            }
                                          }
                                        }
                                      },
                                      child: Text(
                                        "Restore",
                                        style: GoogleFonts.manrope(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 12.sp,
                                            color: Color.fromRGBO(
                                                118, 118, 118, 1)),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: privacyPolicyPressed,
                                      child: Text(
                                        "Privacy Policy",
                                        style: GoogleFonts.manrope(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 12.sp,
                                            color: Color.fromRGBO(
                                                118, 118, 118, 1)),
                                      ),
                                    ),
                                  ],
                                )),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ));
  }
}
