import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../controllers/navigation_bar_controller.dart';
import '../../shared/header.dart';

class Articles8Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Healthy headphone use: How loud and how long?",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "July 22, 2020"
                      "\nBy James Naples, MD, Contributor, and"
                      "Valeria Duque, Au.D., CCC-A, Contributor",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://www.health.harvard.edu/blog/healthy-headphone-use-how-loud-and-how-long-2020072220565",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://www.health.harvard.edu/blog/healthy-headphone-use-how-loud-and-how-long-2020072220565"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles8_1.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "As our society and culture become more connected through technology, the use of headphones has increased. Headphones allow people to enjoy music and have conversations from anywhere at any time. The ease of headphone use and the mobility that they afford cannot be overstated. This is particularly true currently, as our society spends more time with virtual meetings and headphones during the COVID-19 pandemic. Despite the convenience of headphones and the increased utility, questions about safety of use have been raised. There is such a thing as healthy headphone use; you just need to know about safe sound levels and when to take a break from headphones.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How does sound cause hearing loss?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Isn’t sound supposed to provide a tool for communication and awareness of our environment? Yes, sound is an essential mode of communication that also orients us to our environment; however, the inner ear is very sensitive to the balance of sound that it perceives. There are thousands of cells in the ears, some of which have little hairlike structures called hair cells that are responsible for transmitting sound from the ears back to the brain, where it is further processed. Excess sound can cause permanent damage to these cells, which interrupts the mechanism of sound transmission. Damage may also happen via the connection between the hair cells and nerve cells, which can be interrupted by excess sound, even if the hair cells remain normal. In short, one thing is clear: sound that is too loud is harmful.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How loud is too loud?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The CDC has detailed information on various daily experiences and the volume, or decibel (dB) level, associated with them. One of the important things to note when considering headphone use is that personal listening devices are tuned to a maximum volume of around 105 to 110 dB. For reference, exposure to sound levels above 85 dB (equal to a lawnmower or leaf blower) can cause possible ear damage with exposure of more than two hours, while exposure to sound of 105 to 110 dB can cause damage in five minutes. Sound less than 70 dB is unlikely to cause any significant damage to the ears. This is important to know, because the maximum volume of personal listening devices is above the threshold at which damage occurs (in both children and adults)! It is important that as a listener, you are aware that most devices can, in fact, be used in a way that is harmful. Ultimately, personal listening devices should be comfortable to the listener.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Suggestions for safe listening",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Our ears can be damaged by excess sound, and the combination of excess level of sound and duration of exposure contributes to potential hearing problems. Here are some suggestions for healthy listening habits."
                      "\n\n   • Be aware of how long you have been listening and how loud the sound is."
                      "\n   • Take breaks after prolonged listening sessions, and be sure to listen at a comfortable level."
                      "\n   • Be prepared. If you are going to attend an event where there is likely to be prolonged loud noise (such as a concert or sporting event), bring earplugs or headphones. There is a range of devices available that offer protection from a potentially damaging situation, from simple foam earbuds, to headphones with noise cancelling properties, to customizable ear molds made by an audiologist."
                      "\n   • Finally, don’t hesitate to talk with an audiologist or otolaryngologist about any questions you have around headphone use or safe sound levels. Hearing health is important and complex, and we can help you take steps to protect your ears while using headphones.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
