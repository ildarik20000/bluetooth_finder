import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../controllers/navigation_bar_controller.dart';
import '../../shared/header.dart';

class Articles9Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Is Bluetooth Dangerous For Your Brain? Safety & Risks",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Updated on December 9, 2021 - Written by Mitchelle Morgan, Health Writer"
                      "Medically reviewed by Melissa Mitri, MS, RD",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://www.healthcanal.com/mental-health-behavior/is-bluetooth-dangerous-for-brain",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://www.healthcanal.com/mental-health-behavior/is-bluetooth-dangerous-for-brain"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles9_1.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "We live in a world where we are looking for ways to live a better and healthier life. Sometimes we may want to improve our quality of life so much that we look for the best foods, supplements, and even nootropics to build a resilient immunity."
                      "\n\nLiving healthy also means being conscious of what we expose our bodies to. One thing many of us are constantly exposed to is Bluetooth technology. Is Bluetooth safe? In our homes, we all have at least one Bluetooth device. It could be our televisions, portable speakers, Bluetooth headphones, or phones. As a result, no one can completely avoid being exposed to these radiations."
                      "\n\nFortunately, there is a science to help us understand more about Bluetooth radiation and if it is harmful to our brains. So let’s jump right into it.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Is Bluetooth Safe For Brain?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Since Bluetooth is in the non-ionizing group of EMR which emits low-frequency waves, it is safe for humans, and it will not pose any health risks to the brain. These frequencies are too low to change the cell structure. So they cannot dislodge electrons from an atom, which is the dangerous characteristic of ionizing EMRs."
                      "\n\nBluetooth has a meager specific absorption rate, SAR, further proving that it is not lethal to damage the natural cell structure or cause brain cancer."
                      "\n\nSAR is the rate at which our bodies absorb energy per unit mass on energy exposure. Most mobile devices, including Bluetooth headphones and Bluetooth speakers, are manufactured according to a SAR level of 1.6 watts per kg. And this number is even lower for Bluetooth devices."
                      "\n\nThat figure is used in the US; the European SAR level is set at 2 W/Kg."
                      "\n\nHere is a comparison of the levels to actual Bluetooth devices used by people every day:"
                      "\n\n   • Apple Airpods, commonly used Bluetooth earphones have a SAR value of 0.072 Watts per kg for the head and 0.0603 watts per kg for the body."
                      "\n\nBut since these devices still emit radiation, the concern might not be the absorption level but rather how long you are exposed to it. So is long-term exposure to Bluetooth headphones dangerous? We shall take a look at that later on.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "What Is Bluetooth Radiation?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bluetooth is a wireless radio technology that allows two Bluetooth-enabled devices such as Bluetooth headphones to communicate without touching. It facilitates data transfer, allowing you to share photos, music, and other media."
                      "\n\nBluetooth typically works within the frequencies 2.402 to 2.480 gigahertz. And that is quite similar to the same frequencies as Wi-I, mobile phone connections, and microwaves."
                      "\n\nLike these wireless devices, Bluetooth is in the class of electromagnetic radiation, EMR."
                      "\n\nHere is all you need to know about EMR:",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The EMR Spectrum",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The EMR is an invisible area of energy according to the National Institute of Environmental Health Sciences definition. Both synthetic and natural elements may emit or absorb this energy."
                      "\n\nWith EMR, the higher the frequency an object emits, the more energy it releases. There are two main types of EMR: ionizing EMR and non-ionizing EMR.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Ionizing EMR",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Ionizing radiation is the EMR frequency that are notably high and thus can possibly cause brain tumors and other health risks. This is because they can cause DNA damage and also damage human cells."
                      "\n\nIonizing EMR means that these energy molecules are strong enough to dislodge the electrons in an atom of a substance. Exposure to ionizing EMR poses many health risks to the human brain and all other living creatures. Some of the elements that emit EMR radiation are:"
                      "\n   • X-ray machines."
                      "\n   • Sunbeds."
                      "\n   • The sun."
                      "\n   • Radioactive waste.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Non-Ionizing EMR",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Non-ionizing radiation has relatively low emission of electromagnetic radiation, posing only a minimal risk of damage to the cells or DNA."
                      "\n\nExamples of non-ionizing EMR are:"
                      "\n   • Bluetooth wireless devices"
                      "\n   • Mobile phones"
                      "\n   • Wi-Fi networks"
                      "\n   • Energy-smart meters"
                      "\n   • Microwave radiation"
                      "\n   • Electricity lines"
                      "\n   • MRI medical machines"
                      "\n   • Computers and Laptops",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Why People Think Bluetooth Is Dangerous?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Despite scientific evidence against this belief, many people are still concerned that the non-ionizing EMR emitted from Bluetooth speakers is harmful. This is because they continue to emit this radiation. Here are a few of the questions raised:",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Can They Cause Cancer?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Brain cancer is perhaps the most worrying health risk that people have when it comes to using Bluetooth headphones, simply because the devices are often used very close to the brain."
                      "\n\nIn a 2015 study, there was a conclusion that mobile devices have some influence in causing meningioma, a type of brain tumor. This was so much so that it sparked a conversation that Bluetooth devices might have the same effects since they fall under the same group of non-ionizing EMR."
                      "\n\nThe only issue is that t study only touches solely on cell phones increasing the risk of brain cancer. There was never anything noted that Bluetooth devices specifically indeed caused it."
                      "\n\nMore studies  were conducted, but there was no conclusive evidence to support this claim that non-ionizing EMR caused cancer. This further shows that Bluetooth  "
                      "\n\nYet another study further purported that cell phone health risks had no effect even in children."
                      "\n\nAnother study purported that cell phones had possible health risks for childhood cancer. It further stressed that non-ionizing EMR devices or objects that emit this radiation should not be neglected and watched since more studies are needed to prove if Bluetooth is genuinely harmful to kids."
                      "\n\nNow, if you remember, as aforementioned earlier, Bluetooth devices have a lower SAR limit. This means that if cell phones have a minimal effect, then we can be hopeful that Bluetooth devices may pose even less harm to us.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Can Bluetooth Headphones Make You Infertile?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Another primary concern besides brain cancer for Bluetooth devices is fertility. Several studies were linked to the possibility of cell phones causing male infertility And yet again, a few people attributed this to Bluetooth devices."
                      "\n\nIn a 2009 study, there was evidence that with frequent use of mobile devices near the male genitalia, there was a likelihood of sperm count reduction . Then another study showed a similar effect, linking it to increased oxidative stress from the EMR radiations given off by cell phones."
                      "\n\nThese claims may be accurate regarding cell phone use, but it is highly unlikely that someone would use a Bluetooth device near their testes. "
                      "\n\nAnd so there is no proof that Bluetooth devices cause infertility, and is likely just a myth. However, keep in mind that using cell phones near your testes can cause sterility; and the research points to that being factual. So when it comes to family and community health, ensure that you spread the word correctly!",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Does Bluetooth Damage The Brain In Long-Term Exposure?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bluetooth devices emitting any radio waves may not lead to immediate damage to the brain in the short term. However, any concern may be tied to long-term exposure. Is Bluetooth dangerous to the brain long-term?"
                      "\n\nThe answer to this is also likely no."
                      "\n\nBluetooth devices have a relatively low power output, which is the energy it uses to reach the brain. When these Bluetooth radio waves reach the brain, they cannot produce enough heat to cause any lasting neurological disorders. And this still holds true even after using them for long hours, weeks, or months -there is no significant increase in cancer risk."
                      "\n\nBased on the evidence, your Bluetooth devices are likely safe to use, even long-term. In summary, radiation exposure to Bluetooth is unlikely to cook or damage your brain in any way or form.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Why Having Bluetooth Devices Become A Concern?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The concern of the possible adverse health effects of Bluetooth on the brain can be traced back to 2015. Several studies attest that prolonged exposure to mobile phones could harm the human body in several ways during that period."
                      "\n\nThese studies caused such an uproar in the medical community promoting two hundred scientists to appeal to the United Nations and, more specifically, the World Health Organization, WHO, to impose stricter regulations for EMR radiating devices."
                      "\n\nWhat you must note in this scenario is that the focus was never on Bluetooth devices, but on EMR-radiating devices like cell phones."
                      "\n\nIn 2019, when Apple Inc launched its Airpods, another uproar was ignited regarding their safety. However, this claim never explicitly said that the problem was Bluetooth devices. Even so, Bluetooth devices were never on that list in the first place.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Safety Precautions To Take When Using Bluetooth Devices",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Even though we have already debunked this widespread myth that the radio frequency from Bluetooth devices is harmless, it is still better to be safe than sorry. With that, here are some of the things you can consciously do to minimize the risk of negative effects as much as possible.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "1. Avoid Exposure From Multiple EMR Devices.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "It’s true that the SAR levels of your phone and Bluetooth headsets might not be enough to alter the human cell structure. But imagine you are working in the radiation department in a hospital, with your phone and Bluetooth earphones in tow. Is that healthy? We bet not!"
                      "\n\nWhat you are doing to your body is exposing it to various levels of EMR radiation, which collectively increases your overall SAR levels. This might, without a doubt, increase the risk of cancer and other neurological disorders  [21] caused by radiation. And even when you might be eating healthy or chewing gum to stay alert, you are still putting yourself in danger. ",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "2. Opt For Hands-Free Options When Answering Calls.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Yet another way to reduce cancer risk from radiation overexposure is by opting to answer your phone calls with the hands-free option. r Better yet, use your speakerphone. "
                      "\n\nAnother sound choice you have is by using wired headphones that eliminate the use of powering up your phone and headset’s Bluetooth.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "3. Limit Your Child’s Proximity To EMR-Radiating Devices",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Children are more sensitive to radiation in general. In that case, limit how much radiation they get exposed to. You can do this by keeping the phones, Wi-I routers, Bluetooth devices, and microwaves out of their reach. "
                      "\n\nEven exposure to ultraviolet light from the sun can be limited by keeping them away from harsh direct sunlight or simply applying sunscreen on them when they are outside.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "4. Select Brands That Comply With The SAR Limits",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Finally, since SAR limits are fundamental in increasing cancer risks, buy devices that comply with the international limits set forth. For an extra layer of safety, perhaps choose the ones with Federal Communications Commission (FCC) standards too."
                      "\n\nThe FCC allows 3 distinct ISM bands for unlicensed communications systems. The three ISM bands are:"
                      "\n   • 902 to 928 MHz"
                      "\n   • 2.400 to 2.4835 GHz"
                      "\n   • 5.725 to 5.875 GHz"
                      "\n\nThe frequency of Bluetooth/WLAN 802.11 b/g/n gadgets is approximately 2.4 GHz, so they were included in this same lineup of ISM bands.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bottom Line",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bluetooth devices such as headsets, speakers, and Bluetooth headphones are not inherently harmful to cells and are unlikely to cause brain cancer. They have their own set of advantages, such as allowing you to maintain your concentration without being restricted by wires."
                      "\n\nIt’s an added bonus that you can dance without tagging on a wire and work and move without being restricted by the wires too. "
                      "\n\nscience is constantly evolving with emerging studies, and so there is still a lot of research underway. And so, this calls for you to proceed with caution when it comes to all electronic devices.n, Follow the precautionary steps above to fend off any possible health conditions that may be caused by radiation exposure.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
