import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../controllers/navigation_bar_controller.dart';
import '../../shared/header.dart';

class Articles12Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bluetooth Trackers: How It Works",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://chipolo.net/en/blogs/bluetooth-trackers-how-it-works",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://chipolo.net/en/blogs/bluetooth-trackers-how-it-works"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How many times a week do you look for your lost keys, wallet or phone?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "We’ve all done it and every time we do, we promise ourselves that this was the absolute last time. Sound familiar? Well, it turns out there’s a simple and affordable solution to the problem. It’s called a Bluetooth tracker.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "What is a Bluetooth Tracker?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "A Bluetooth tracker is a small gadget you can attach to your things to keep an eye on them. It uses Bluetooth Low Energy (also known as BLE or Bluetooth 4.0) to wirelessly connect to your mobile device and transmit data to that device through the connection.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How Does A Bluetooth Finder Work?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Each Bluetooth finder tag has a unique signature that is only known to its owner once they connect the Bluetooth gadget to their user account on the accompanying mobile app."
                      "\n\nThis way, the item finder and the app can recognize each other, ensuring the tracker cannot be tracked by outsiders and its location is only known to the owner. The only exception to this rule is when the owner decides to share their key or wallet tracker with another user within the app, giving them permission to track the finder from their account as well."
                      "\n\nBecause this connection is set up with Bluetooth Low Energy, both the device and the gadget use a truly negligible amount of power."
                      "\n\nThe tracker can last anywhere upwards of 12 months and your phone can last all day without you having to worry it will run out of power. Both the key and wallet tracker have a few basic components that you can find in any model Bluetooth tracking tag.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Every Bluetooth tracking tag has an internal power source (a battery), a chip and an antenna.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Power SourceThis is almost always a small lithium-ion battery (also known as Li-ion battery or LIB). The batteries range from the standard coin-shaped ones to custom-made batteries that specifically fit the shape of their respective Bluetooth key tracker. These batteries can be rechargeable, which eliminates the additional cost of having to replace the battery, but also means shorter periods between charges as rechargeable batteries usually don’t last as long as replaceable or non-replaceable batteries do."
                      "\n\nMost commonly, the replaceable or non-replaceable battery used is the CR2025 or CR2032 coin-shaped one. Whether the battery is replaceable or not depends on the company that made the smart finder tag and on the features that a particular model Bluetooth tracking tag offers. For example, water-resistant Bluetooth finders, like the Chipolo ONE, have a replaceable battery, but their insides are protected by a casing to ensure the tracker stays water-resistant.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Chip",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Each tracker has a tiny chip inside it and this is the brains of the operation. It’s the chip that makes the Bluetooth finder tag ring, deals with location information and allows you to set up individual Bluetooth tracker features according to your preferences (like setting up different ringtones on the Chipolo Bluetooth finders)."
                      "\n\nChipolo key and wallet trackers also have a backup chip, to help reset the main chip if it’s ever needed. Despite all the tasks that the chip performs, it’s still set up to use as little power as possible and make the tracker last longer on a single battery charge.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Antenna",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Every Bluetooth finder has an antenna that allows it to communicate all the information from the chip with the mobile devices the finder is registered to, through a radio-based link. The antenna is fine-tuned to ensure the item tracker reaches the best connection range possible with your device and uses as little power as possible doing so.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Is A Bluetooth Tracker Tag For Me?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "One important thing to note is that Bluetooth trackers are different from GPS trackers. GPS tracking technology provides location updates in real-time, and does so through third-party connections, while Bluetooth tracking technology relies on your mobile device to update the location of the item it is attached to.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "What will I be using the finder tag for?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If the answer is to track your car then a GPS tracker, although a bit pricier, is what you’re after. If, on the other hand, you want the finder tag to help you find your keys or wallet in your home then a Bluetooth tracker is the perfect solution for you.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
