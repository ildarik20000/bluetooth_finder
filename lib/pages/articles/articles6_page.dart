import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../controllers/navigation_bar_controller.dart';
import '../../shared/header.dart';

class Articles6Page extends StatelessWidget {
  Articles6Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How to Clean AirPods, Earbuds, and Headphones",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://www.nytimes.com/wirecutter/guides/how-to-clean-airpods-earbuds/",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://www.nytimes.com/wirecutter/guides/how-to-clean-airpods-earbuds/"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles6_1.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Headphones and earbuds can get gross. Skin cells, sebum, ear wax, and facial products cling to the parts that touch your body. Increasingly sticky headphones in turn pick up more dirt from the world around you every time you put them down. And despite what the internet says, no, sucking on your AirPods is not a safe way to clean them. Doing so could damage the driver, protective mesh, and electronics (also, it’s just plain nasty). But there are easy ways to properly clean your personal audio devices (for your own well-being and before lending or borrowing a pair)."
                      "\n\nBonus motivation: When your headphones are clean, they will not only last longer but also sound better (in the case of earwax- or lint-clogged earbuds). With that in mind, here are a few tips on how to appropriately clean and care for your headphones.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "What you need",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "\n\n   1.  Soapy water: It’s best to use dish soap (we like Seventh Generation Dish Liquid) because it wipes cleaner than many hand soaps (which may have moisturizers and added colors)."
                      "\n   2. A soft cloth: A microfiber cloth made for polishing glass or glasses won’t scratch your device."
                      "\n   3. Earbud cleaning tool: Literally designed to get the gunk out of earbuds, this tool is your best asset for unblocking sound tubes."
                      "\n   4. Silica gel pack: For gym bags or humid climates, this will keep your headphones dry and fresh."
                      "\n   5. Rubbing alcohol: Used sparingly and with caution, this will make the silicone eartips and hard plastic parts of your device germ-free.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles4_1.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you don't see your AirPods in the Bluetooth  or volume control  menu,2 pair your AirPods with your Mac:"
                      "\n\n   1.  On your Mac, choose System Preferences from the Apple menu, then click Bluetooth ."
                      "\n   2. Make sure that Bluetooth is on."
                      "\n   3. Put both AirPods in the charging case and open the lid."
                      "\n   4. Press and hold the setup button on the back of the case until the status light flashes white."
                      "\n   5. Select your AirPods in the Devices list, then click Connect.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How often should you clean your headphones?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "There is no hard-and-fast rule as to how frequently you need to clean your earbuds or headphones. The frequency can change based on how often you wear your ear gear, what you’re doing while wearing it, and the climate you live in."
                      "\n\nFor example, headphones you use daily and keep on your desk should be wiped down every week or so. Gym earbuds may need to be wiped down after every use, particularly if you sweat a lot."
                      "\n\nThe best advice is to keep an eye on your stuff. When you notice some earwax, dust, sticky fingerprints, oil, or sweat on headphones, clean them right away. Putting this off will only lead to more gunk and make your job harder.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How to clean AirPods, earbuds, and headphones",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Regularly wipe down your headphones or earbuds. Ideally, you should wipe down your headphone earpads or earbud tips with a lightly damp cloth at least once a week. Make sure the headphones are powered off and disconnected from your device. Immediately dry them fully with a soft cloth. Don’t use alcohol because it can remove color or break down leather or fabric faster. Soap and water will clean off any nasties.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Clean the inside of earbuds with a small, soft brush to remove debris.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Wax buildup can muffle the sound of your earbuds. Get an inexpensive cleaning tool designed for clearing out wax (some high-end in-ear models even come with one)."
                      "\n\nFirst, pull the eartips off the earbuds. Then use the tool’s metal-loop side to gently scoop out any ear wax."
                      "\n\nDo not jam the loop into the tip while it’s still on the earbud or you may push wax into the earbud and damage the driver."
                      "\n\nNext use the small, soft brush end of the tool to gently clean off anything stuck to the earbuds. Be sure to aim the earbud opening toward the ground to allow gravity to assist you and also to ensure small bits of loosened wax aren’t falling into the earbuds themselves. Wipe the eartips with a soapy, wet cloth and dry thoroughly before reattaching to the earbuds.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Clean the charging case and battery connections.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "For true wireless earbuds, like AirPods, that are stored in a charging case, it’s important to regularly clean the case and the connectors."
                      "Most wireless earbuds charge via small pins and metal pads that can get gunked up with earwax and dust. When this happens, you may find that your earbuds won’t charge properly."
                      "To clean the inside of the case, Apple recommends wiping both the AirPods case and earbuds with a dry cloth. We like to use a cotton swab that’s dampened (not dripping!) with isopropyl alcohol to wipe clean the pins on the inside of the case and the earbud connectors. You can also use this method on the charging ports for your over-ear headphones, if they don’t seem to be charging consistently. The brush side of the tool we mentioned above will work wonders for little USB-C and Micro-USB ports, which can get clogged with pocket lint.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Clean the charging case and battery connections.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "After every workout, wipe down your headphones, and allow them to dry fully before charging."
                      "Even if your headphones are water- and sweat-resistant, don’t let them remain wet for long periods because water can slowly seep into battery compartments or the earbuds themselves and short them out or damage the drivers. Dry off true wireless earbuds before placing them into their charging case. (Even swim headphones should be allowed to dry fully before you put them away.) Do not allow moisture into the earcups of over-ear headphones or into the nozzles of in-ear headphones."
                      "If you need to take your headphones out mid-run, or you can’t wait for them to dry completely before leaving the gym, gently shake out any excess moisture, turn off the power, and store them inside the included case or a little zip-close snack baggie. Don’t keep your earbuds in your gym-shorts pocket or sports bra without a water-resistant case. Both of these places collect sweat and won’t allow the earbuds to dry as fully as they need to. Then when you arrive home, wipe the earbuds, and allow them to dry as stated above.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How to store your headphones so they stay cleaner longer",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Clean the charging case and battery connections.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Store them in a bag or case when they’re not in use."
                      "This will protect them from dirt, scratches, and impacts. Use the case you got when you bought them, or you can buy one later. Just be sure the case you choose will fit the headphones you have.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Don’t use or store your headphones in very hot or cold environments.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Saunas and car trunks aren’t good for headphones, especially if your pair has a battery. Also, don’t store them for a long time in a humid environment or when they’re wet, even if they are water-resistant. For extra protection, keep a silica gel pack in the bag (just be sure to keep gel packs away from kids and pets).",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If your headphones are powered, don’t leave the charge port or battery compartment open when storing them.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Even if your headphones won’t get wet, leaving the port open can let in dirt and dust. If your headphones don’t have a door, and they have just a Micro-USB or USB-C port for charging, regularly dust out the port as described above. If you’ve been working out or walking in the rain, check for water inside the port before plugging in.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "With wired earbuds and headphones, don’t kink or bunch the cable or wrap it around your device while it’s still plugged in.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Wrapping your headphones around your phone while they’re still plugged in bends the cable where it meets the plug end at a sharp angle and can eventually pull it out of the housing. To wrap the cord properly, gently loop it around your fingers, making a circle with the cable following the wire’s natural curve. To prevent tangling, use a twist tie from a loaf of bread, or get a little Velcro cable tie.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Always unplug your headphones or charging case by holding the plug or cable end, not the cable itself.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Don’t unplug or remove headphones from your device by pulling the cable because this can weaken the cable over time. And definitely don’t remove wired earbuds from your ears by pulling on the cable, especially if they’re sealed; the vacuum created can cause damage to your ears. Instead, grasp the earbud and twist slightly while pulling outward."
                      "When you follow these guidelines, your headphones will stay cleaner and in better shape—and they will last you a lot longer. Happy listening!",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
