import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../controllers/navigation_bar_controller.dart';
import '../../shared/header.dart';

class Articles7Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "8 Ways You're Using Your Headphones Wrong",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://www.pcmag.com/how-to/ways-youre-using-your-headphones-wrong",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://www.pcmag.com/how-to/ways-youre-using-your-headphones-wrong"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Do your headphones or earphones keep breaking? Or are they just not working as well as they should be? Follow these tips to end the painful cycle of buying a replacement pair every year.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles7_1.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Let me guess: You rarely invest much money in headphones or earphones because they always break and you end up buying a new pair every year or so. Variations on this theme are regularly presented to me, often as an excuse for not buying a decent pair, or sometimes accompanied by the question: Why do my headphones always break?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "It's time for some tough love. The answer to that question, 99% of the time (and as you probably already know), is a resounding: You.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The headphones don't break. You break the headphones. It's OK—you obviously don't mean to! Life is busy and overwhelming and who has time to lovingly coddle some earbuds that need to be stuffed into your bag or pocket ASAP as you scramble to catch the train? Luckily, there are a number of ways to avoid the early demise of your headphones and get far more than a year's worth of life out of them.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Here's what you're doing wrong, and how to fix it.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles7_2.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "1. Forgetting to Clean",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Earwax. A gross word. You don’t want to read it, I don’t want to write it. Seeing actual earwax on the surface of—or worse, jammed inside—the eartips of your earphones is an unfortunate reality of wearing in-ears. And because of the recent ubiquity of charging cases, earwax on true wireless earbuds can do some real damage if not properly cleaned—you don’t want it to transfer to the inside of the charging case and build up gradually over time."
                      "\n\nNow, some true wireless pairs are fully waterproof or highly water resistant, and can be quickly rinsed off under the faucet, problem solved. But anything with a rating of IPX5 or lower (we’ll discuss this in the next section) is a gamble to clean that way—and that includes many of the big names in the true wireless realm, like all of the AirPods, and top models from Bose, Jabra, and Sony."
                      "\n\nFor these models, you have a couple methods worth trying. Most silicone eartips can be easily removed, and when not attached to the earpiece, can be run under water without issue. But you’ll have to thoroughly dry them before reconnecting them to the earpiece—use a dry microfiber cloth or something else soft and free of lint."
                      "\n\nThere are also earwax cleaning tools. You need to be careful with them, as they are mostly made by third-party manufacturers and aren’t typically designed to work with any specific model of earphones. But they're simple, affordable tools that can make a world of difference.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles7_3.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "2. Misunderstanding IP Ratings",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "\"Water-resistant\" and \"waterproof\" are terms that often get used interchangeably, but they shouldn't be. What's needed—and what some manufacturers are loathe to share—is the product's IP rating.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "IP stands for Ingress Protection, and after the letters IP there should be two digits. The first digit refers to protection from solids, like dust. 0 means no protection, 6 means total protection, and X typically means that, while the manufacturer didn't necessarily test for protection against solids, the assumption is that the product has some level of protection. In other words, X in an IP rating means something like \"better than 0, but beyond that, we're not sure so don't sue us.\""
                      "\n\nThe second digit in an IP rating refers to protection against liquids. 0 is, again, nothing, and 8 is excellent—that means the product can be submerged up to 1 meter (perhaps beyond) and withstand some fairly high-pressure water from, say, a faucet or a torrential downpour, and not suffer damage. So, IPX8 means you can assume the protection against solids is better than zero, and the protection against liquids is top-notch. IP68 means the product is as protected from ingress of the solid and liquid varieties as can be."
                      "\n\nManufacturers sometimes say their products are water-resistant without listing an IP rating, and then when pressed for an IP rating, often produce an underwhelming rating of, say, IPX4. IPX4 essentially means that low-pressure water won't harm the headphones—but we're talking about light rain, sweat, or mist from a spray bottle. Rinsing the headphones under a faucet could certainly do some damage. And dunking them in the pool can, too."
                      "\n\nSo if you're using your headphones for exercise at the gym, in the rain, near the pool, and rinsing them off after, they need an IP rating like IPX7 or IPX8. If your \"waterproof\" earphones are regularly dying after a few visits to the gym, it's possible that in reality they're only rocking an IPX4 rating."
                      "\n\nIf you don't know your product's IP rating and it's not in the manual or online, you can always try asking the manufacturer directly or posting a question on the product's webpage. The manufacturer knows the rating—and if it's IPX7 or IPX8, it's probably already listed because they know it's a selling point."
                      "\n\nNow, keep in mind that when it comes to true wireless earphones, the IP rating applies only to the earpieces themselves. If you put damp earpieces into a charging case, there’s a good chance there will be problems in the near future. If you put clearly wet earpieces into a charging case, I give your earphones a day or two before one or both starts to fail. Earpiece failure can sound like distorted audio, or it can take the more likely form of the earpiece simply dying. So take extra care with true wireless earphones, which tend to be more expensive and delicate than other models.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles7_4.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "3. Blasting the Volume",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Whether we’re talking about wireless in-ears or wired studio headphones—yes, sometimes drivers do fail."
                      "\n\nThis is rare, and it might just mean the headphones have lived a long life and gotten as much usage as the drivers can handle, like an old car engine. If the drivers suddenly sound damaged, however—distorting on various types of music, even genres without deep bass, and at volume levels that shouldn't cause distortion—it's more likely that the headphones were blasted at overly high volumes. Just as you can blow a speaker out this way, you can blow your headphone's drivers."
                      "\n\nThis is far less likely and common when using mobile devices as sound sources, but it's still possible. Stereo gear or studio equipment outputs can drive your headphones to much higher levels, and at some point, driver damage can occur. The good news is, you can avoid this by just keeping the volume at moderate levels. And hey, bonus points: You'll avoid damaging your ears this way, too."
                      "\n\nOne way to avoid accidentally blasting your drivers to smithereens is to always lower the volume on your mobile device or stereo when powering it down or taking the headphones off. Adjust the volume once you're plugged back in, and you might even find yourself listening at lower levels in general, which, incidentally, is another excellent way to preserve your hearing.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles7_5.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "4. Wadding Up Wires",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The most common way to destroy wired headphones is by mistreating the cable. The real tragedy here is that the drivers inside the headphones are likely doing just fine—all they need is a cable to deliver the audio. But in models that have hardwired cables, cable damage is often a death sentence. This isn't always true—Sennheiser, for instance, makes plenty of headphones that have what appear to be hardwired cables, but with some effort, manual consultation, and patience, can actually be replaced (and, of course, if you or someone you know has soldering skills, then just about every cable can be replaced)."
                      "\n\nBut the better solution for those who lack this type of patience is to start with a pair of headphones that have a removable (read: unpluggable) cable. They often cost more, but not always. In fact, many Bluetooth models come with removable cables so they can be used passively in wired mode. But let's assume that you don't want to keep buying replacement cables your whole life..."
                      "\n\nWhy do cables fail in the first place? One common culprit is the wiring getting severed at the connection points—either to the earcups/earpieces, or at the 3.5mm plug. On the outside, the cable could look fine, but internally, there are severed ligaments. The way to avoid breakage is to manage the tension. You should never see your cable turning a 90-degree angle directly where it's plugged in. The internal wires are straining to maintain their connection with the plug, and you're pulling them away, whether you mean to or not. When plugged in, a cable should ideally have no tension at all, but should rest loosely with no obvious pull at either end."
                      "\n\nAs for storing the cable, it's all about loops. Internally, cables have natural coils, and by their very nature, they want to follow these coils. A new cable should be relatively easy to wind up in tidy, equally sized loops that correspond to the natural coil of the internal wiring. Even if you can't feel where the coil of the cabling wants to go naturally, keeping it in loose, equally sized loops and then securing it all with a twist-tie or Velcro cable tie will help your cable live its best, longest life (but don't use the cable's plug end as its own tie, as this just causes tension).",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles7_6.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "5. Not Using a Case",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Another way to keep the cable looped is to use the pouch that likely came with your earphones or headphones when you bought them. Several of these cases are semi-hard and circular, designed to hold, you guessed it, a properly looped cable. Even if you lost the case or the manufacturer didn't include one, finding a small pouch of some sort that you don't have to cram the headphones into in order for them to fit will help protect the cabling, pad the earpieces and earcups, and hopefully keep them from being crushed when you absentmindedly throw your bag on the floor after a long day of work.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles7_7.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "6. Being Too Pet-Friendly",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Once, when I first got my cat, Willie, I was a little foolish and left my studio headphones on the floor in my bedroom, a mistake you make once. Young Willie proudly presented me with a wireless version of those headphones later in the evening—he sawed the cable in half with his sharp baby kitten teeth. It was a gift I deserved—that's no way to treat quality headphones."
                      "You can file this one under \"common sense,\" but keep your headphones and earphones away from your curious pets, and for that matter, keep them off the floor in general. Other than the time my carry-on bag was crushed, that's the only time I ever destroyed a pair of headphones. We might file the carry-on incident under \"bad luck,\" but they were in the outer pocket of a duffel bag—I probably could have found them a more cushioned spot in my luggage.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles7_8.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "7. Ignoring the App",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Many headphones these days work with apps. In addition to adjustable EQ, the best apps usually have settings sections where you can customize the controls. It’s hard to overstate what an advantage this is. Do you never use your phone's voice assistant, for example? Well, instead of one of the on-ear controls summoning Siri, they can be assigned to change the volume. And if your headphones always annoy you with a feature you can live without (like automatically playing music the second you put them on), there’s a good chance you can disable it in the app."
                      "As for AirPods, which lack an app, you can usually adjust some things in the Bluetooth Settings menu on your phone or computer. In iOS, just tap on the “i” button next to your AirPods on the Bluetooth device list.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles7_9.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "8. Not Spending Enough",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  InkWell(
                    onTap: () {
                      final settingsController = Get.put(NavigationBarController());
                      settingsController.articlesPage.value = 13;
                    settingsController.update();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: RichText(
                          text: TextSpan(
                              text:
                                  "And now we come to the simple truth we all know in our hearts: Cheaper things cost less money to make, and are often made with lower-grade materials and less attention to detail and overall quality. This truth applies to wired, wireless, and cable-free headphones."
                                  "\n\nThe good news is, in recent years, cheap headphones have started to sound a whole lot better—the ability for inexpensive drivers to reproduce powerful (or somewhat powerful) bass response is one of the main reasons. No one will mistake a \$30 pair for a \$300 pair, but it's possible to get far better-sounding headphones on a budget than it used to be."
                                  "\n\nBut if I had to make a bet, I'd be putting my money on the \$300 pair outlasting the \$30 pair. For that matter, I'd wager that plenty of \$100 pairs will outlast most \$30 pairs. Electronics aren't made to last forever, but when the materials and components actually have a semblance of value, and they're assembled with higher standards of quality control than we'll typically see in a budget product, there's an excellent chance those headphones are going to have a longer life. There's another factor at play, as well. If you spend more on a pair of headphones, you're more likely to take better care of them, perhaps out of paranoia that you'll break them."
                                  "\n\nUltimately, go with what you can afford, just don't get the absolutely cheapest option unless you're prepared to buy it again a few times. Invest in quality headphones with removable cables, and treat them well—you'll spend less money in the long run and enjoy better audio as a result."
                                  "\n\nFor more, check out our tips on ",
                              style: GoogleFonts.manrope(
                                  color: Theme.of(context).hoverColor,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 15),
                              children: [
                            TextSpan(
                              text:
                                  "how to get the most out of your headphones,",
                              style: GoogleFonts.manrope(
                                  decoration: TextDecoration.underline,
                                  color: Color.fromRGBO(0, 122, 255, 1),
                                  fontWeight: FontWeight.w400,
                                  fontSize: 15),
                            ),
                            TextSpan(
                              text:
                                  " from keeping them clean to improving their sound quality.",
                              style: GoogleFonts.manrope(
                                  color: Theme.of(context).hoverColor,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 15),
                            )
                          ])),
                    ),
                  ),
                  // Container(
                  //   padding: EdgeInsets.symmetric(horizontal: 24),
                  //   child: Text(
                  //     "And now we come to the simple truth we all know in our hearts: Cheaper things cost less money to make, and are often made with lower-grade materials and less attention to detail and overall quality. This truth applies to wired, wireless, and cable-free headphones."
                  //     "\n\nThe good news is, in recent years, cheap headphones have started to sound a whole lot better—the ability for inexpensive drivers to reproduce powerful (or somewhat powerful) bass response is one of the main reasons. No one will mistake a \$30 pair for a \$300 pair, but it's possible to get far better-sounding headphones on a budget than it used to be."
                  //     "\n\nBut if I had to make a bet, I'd be putting my money on the \$300 pair outlasting the \$30 pair. For that matter, I'd wager that plenty of \$100 pairs will outlast most \$30 pairs. Electronics aren't made to last forever, but when the materials and components actually have a semblance of value, and they're assembled with higher standards of quality control than we'll typically see in a budget product, there's an excellent chance those headphones are going to have a longer life. There's another factor at play, as well. If you spend more on a pair of headphones, you're more likely to take better care of them, perhaps out of paranoia that you'll break them."
                  //     "\n\nUltimately, go with what you can afford, just don't get the absolutely cheapest option unless you're prepared to buy it again a few times. Invest in quality headphones with removable cables, and treat them well—you'll spend less money in the long run and enjoy better audio as a result."
                  //     "\n\nFor more, check out our tips on how to get the most out of your headphones, from keeping them clean to improving their sound quality.",
                  //     style: GoogleFonts.manrope(
                  //         height: 1.6,
                  //         fontWeight: FontWeight.w400,
                  //         fontSize: 15,
                  //         color: Theme.of(context).hoverColor),
                  //   ),
                  // ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
