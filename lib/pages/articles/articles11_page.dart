import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../controllers/navigation_bar_controller.dart';
import '../../shared/header.dart';

class Articles11Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "What is a Bluetooth tracker?",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://chipolo.net/en/blogs/what-is-a-bluetooth-tracker",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://chipolo.net/en/blogs/what-is-a-bluetooth-tracker"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "In short, a Bluetooth tracker is a small smart gadget that you can attach to your personal belongings and connect to your phone using Bluetooth. It helps you track your things and locate them by ringing them when they get misplaced or lost.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bluetooth trackers are sometimes also referred to as Bluetooth finders or Bluetooth tags and more generally, smart trackers or tracking tags. The latter two are also used to describe GPS trackers, so pay attention to what technology a certain tracker is using before you buy.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How does a Bluetooth tracker work?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bluetooth trackers are usually used to find keys, car keys, wallets, bags, backpacks, toys, TV remotes, and even small pets."
                      "\n\nA tracker will connect to an app on your phone via Bluetooth the first time you set it up and then maintain a connection to your phone for as long as it is in the connection range (60 meters / 200 feet)."
                      "\n\nYou make it ring by pressing a button in the app on your phone, which makes the Bluetooth key finder ring with a loud melody to help you find your missing item.  "
                      "\n\nOnce the device and your phone are out of connection range, the app on your phone will display the last location the tracking tag was connected on a map instead."
                      "\n\nThe Bluetooth connection it has with the phone works both ways, so you’ll also be able to use your smart tracker to find your phone; even if it’s on silent.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Basic Features of Bluetooth Trackers",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w700,
                          fontSize: 19,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The smart tag has a single battery inside and, in some cases, it can be replaced.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The tracker saves power as much as possible to make it last longer, so it relies on your phone to do all the ‘heavy lifting’ when it comes to data transmissions. Some Bluetooth trackers have a replaceable battery, so you don’t have to replace the whole tag when the battery runs out, while others offer a discounted renewal program for when you have to repurchase the tag.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The connection range on a Bluetooth tracking device can go up to 60 meters (200 ft).",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "It is limited by the Bluetooth signal strength of your phone but unlike most GPS trackers, it will keep the connection even when indoors, so they are perfect for tracking everyday essentials like keys or wallets, but not the best choice for tracking your car or child over larger distances.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The Bluetooth key finder also has Out of Range Alerts for your phone.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "When these are enabled, you'll receive a notification on your phone whenever you leave your keys or your wallet behind."
                      "\n\nHowever, with Bluetooth finders, this feature doesn’t support geofencing like GPS trackers do."
                      "\n\nIt will notify you when you go out of range of your Bluetooth tracker, so you can go back and take your keys or wallet with you before it is too late or you're too far away.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Most Bluetooth trackers can also be shared with your loved ones.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If both of you have the accompanying app on your phone, you can virtually share the Bluetooth tracker, so it is easier to find if it ever gets lost."
                      "\n\nThis comes in handy with shared items like your car keys or when you're trying to find your child's favorite toy that they lost (again)."
                      "\n\nThe tracker will only connect to one phone at a time (usually the one that is closest), but you will both be able to see its location on a map in the app and see where it was last connected to a phone and whose phone it was connected to.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "But wait, there's more...",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The basic features of Bluetooth trackers that we described above (e.g. ring your lost keys/ring your phone) are more or less the same with all brands, but each brand also offers a few additional features that they see could be important to their users (water resistance, custom ringtones, web apps to help you find your phone from your computer, different colors…)."
                      "\n\nMake sure to browse for any additional features that the tracker or its app offer and if these are free or if they come with a monthly subscription attached.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bonus hint:",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Have a look at the ratings of the app that accompanies the Bluetooth tracker too and how responsive their customer support is. These are usually good indicators of how invested the company is in their product!",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
