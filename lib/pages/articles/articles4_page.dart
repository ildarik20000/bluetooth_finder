import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../config/colors.dart';
import '../../controllers/navigation_bar_controller.dart';
import '../../shared/block_faq.dart';
import '../../shared/header.dart';

class Articles4Page extends StatelessWidget {
  Articles4Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Set up AirPods with your Mac and other Bluetooth devices",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://support.apple.com/en-us/HT208718",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://support.apple.com/en-us/HT208718"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Learn how to set up your AirPods with your Mac, Android device, or another Bluetooth device to listen to music, take phone calls, and more.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Use AirPods with your Mac",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you're using AirPods (2nd generation), make sure that your Mac has macOS Mojave 10.14.4 or later. AirPods Pro require macOS Catalina 10.15.1 or later. AirPods (3rd generation) require macOS Monterey or later."
                      "\n\nIf you set up your AirPods with your iPhone and your Mac is signed in to iCloud with the same Apple ID, your AirPods might be ready to use with your Mac. Place your AirPods in your ears and click either the Bluetooth menu  or the volume control  in the menu bar on your Mac.1 Then choose AirPods from the list.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles4_1.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you don't see your AirPods in the Bluetooth  or volume control  menu,2 pair your AirPods with your Mac:"
                      "\n\n   1.  On your Mac, choose System Preferences from the Apple menu, then click Bluetooth ."
                      "\n   2. Make sure that Bluetooth is on."
                      "\n   3. Put both AirPods in the charging case and open the lid."
                      "\n   4. Press and hold the setup button on the back of the case until the status light flashes white."
                      "\n   5. Select your AirPods in the Devices list, then click Connect.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles4_2.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If sound still plays from your Mac speakers, click either the Bluetooth menu  or the volume control  in the menu bar and make sure that your AirPods are selected as the output device.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Use AirPods with a non-Apple device",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "You can use AirPods as a Bluetooth headset with a non-Apple device. You can't use Siri, but you can listen and talk. To set up your AirPods with an Android phone or other non-Apple device,3 follow these steps:"
                      "\n\n   1.  On your non-Apple device, go to the settings for Bluetooth and make sure that Bluetooth is on.4 If you have an Android device, go to Settings > Connections > Bluetooth."
                      "\n   2. With your AirPods in the charging case, open the lid."
                      "\n   3. Press and hold the setup button on the back of the case until you see the status light flash white."
                      "\n   4. When your AirPods appear in the list of Bluetooth devices, select them.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Switch devices automatically",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Your AirPods (2nd generation), AirPods (3rd generation), and AirPods Pro can switch automatically between your Mac with macOS Big Sur, iPhone with iOS 14, and iPad with iPadOS 14 or later. Your devices need to be signed in with the same Apple ID using two-factor authentication. For example, while listening to music on your Mac, you answer a call on your iPhone. Your AirPods automatically switch from the music on your Mac to the phone call on your iPhone."
                      "\n\nIn some situations, you'll see a notification about your AirPods on the screen of your Mac. When you hold the pointer over the notification, a Connect button appears. Click Connect to confirm that you want your AirPods to switch to—or stay with—your Mac."
                      "\n\nIf you want to turn automatic switching off for your Mac, open Bluetooth preferences on your Mac with your AirPods in your ears. Click the Options button next to your AirPods in the list of devices. Click Connect to This Mac, and then choose When Last Connected to This Mac. To turn this feature on again, choose Automatically."
                      "\n\nSound should not automatically switch from one device to another if you're in a conversation, like a phone call, a FaceTime call, or a video conference.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
