import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../config/colors.dart';
import '../../controllers/navigation_bar_controller.dart';
import '../../shared/block_faq.dart';
import '../../shared/header.dart';

class Articles3Page extends StatelessWidget {
  Articles3Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Find your lost AirPods",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://support.apple.com/en-us/HT207581",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://support.apple.com/en-us/HT207581"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Use Find My to see your AirPods, AirPods Pro, or AirPods Max on a map and play a sound to locate them."
                      "\n\nIf you already set up Find My with an iPhone, iPad, iPod touch, or Mac that you're using with your AirPods, Find My can help you locate them. If you didn't turn on Find My before your AirPods went missing, you can't use Find My to locate them."
                      "\n\nFind My is the only way that you can track or locate a lost or missing device. There is no other Apple service that can find, track, or otherwise flag your device for you. Learn how to set up Find My.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "See your AirPods on a map",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you've lost or misplaced your AirPods, you might be able to find them if you use the Find My app on an iPhone, iPad, iPod touch, or Mac that you're signed in to with the same Apple ID.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "See your AirPods in the Find My app",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Update to the latest version of iOS or iPadOS, or macOS."
                      "\n   1.  Open the Find My app."
                      "\n   2. Select the Devices tab."
                      '\n   3. Choose your AirPods. Under each device, you see a location or "No location found."'
                      "\n   4. To use your computer to find your AirPods, use Find My on iCloud.com.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Turn on the Find My network",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you turn on the Find My network, you might be able to see the location of your AirPods even if your iPhone, iPad, or iPod touch isn't connected to a Wi-Fi or cellular network:"
                      "\n\n   1.  On your iPhone, iPad, or iPod touch, open the Settings app."
                      "\n   2. Select your name, then select Find My."
                      "\n   3. Select Find My [device], then turn on Find My [device]."
                      '\n   4. Turn on "Find My network."',
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you need to find only one AirPod or AirPod Pro",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If your AirPods or AirPods Pro are separated from each other, you see only one location at a time on the map. Find the AirPod or AirPod Pro that you see on the map, and put it in its case. Then refresh the map and find the other one.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Play a sound on your AirPods",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If your AirPods are near any of your Apple devices and connected to Bluetooth, you can play a sound to help you find them from iCloud.com or the Find My app:"
                      "\n\n   1.  Open the Find My app."
                      "\n  2. Select the Devices tab."
                      "\n  3. Choose your AirPods from the list."
                      "\n  4. Tap Play Sound to play a sound that gets louder gradually.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles3_1.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Make sure that your AirPods are not in or on your ears before you play a sound.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Get an alert if you leave your AirPods behind",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "You can set up separation alerts so that you don't leave your AirPods (2nd generation) or later, AirPods Pro, or AirPods Max at an unknown location:"
                      "\n\n   1.  On your iPhone, iPad, or iPod touch, open the Find My app."
                      "\n   2. Go to the Devices tab, and choose your AirPods."
                      "\n   3. Under Notifications, tap Notify When Left Behind."
                      "\n   4. Turn on Notify When Left Behind.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles3_2.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Turn on Lost Mode for your AirPods",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "When you turn on Lost Mode for your AirPods (2nd generation) or later, your AirPods Pro, or your AirPods Max, you can share a message with your phone number or email address. If someone finds your AirPods, they can see the message on their iPhone."
                      "\n\nTurn on Lost Mode:"
                      "\n\n   1.  On your iPhone, iPad, or iPod touch, open the Find My app."
                      "\n   2. Go to the Devices tab, and choose your AirPods."
                      "\n   3. Scroll down to Mark as Lost, and select Activate."
                      "\n   4. Follow the onscreen steps if you want your contact information to display for your missing AirPods."
                      "\n   5. Select Activate.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles3_3.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If your AirPods are offline",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      'If your AirPods are out of range or need to charge, you might see their last known location, "No location found," or "Offline.'
                      "\n\nYou can't play a sound to find them, but you might be able to get directions to the location where they were last connected. If they do come back online, you get a notification on the iPhone, iPad, iPod touch, or Mac that you use with them.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Replace a missing AirPod or charging case",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you still can't find your AirPod or charging case, or if something is damaged, get the serial number and then contact Apple Support for a replacement. Then learn how to set up your replacement."
                      "\n\nYou can also learn more about AirPods pricing and service options.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
