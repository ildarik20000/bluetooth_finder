import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../config/colors.dart';
import '../../controllers/navigation_bar_controller.dart';
import '../../shared/block_faq.dart';
import '../../shared/header.dart';

class Articles5Page extends StatelessWidget {
  Articles5Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "5 Easy Tips to Extend the Life of Your Headphones",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://www.pcmag.com/how-to/5-easy-tips-to-extend-the-life-of-your-headphones",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://www.pcmag.com/how-to/5-easy-tips-to-extend-the-life-of-your-headphones"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Keep your headphones working longer and improve the way they sound with these simple tricks."
                      "\n\nJust bought a pair of headphones you love and want to keep them looking and sounding as good as new? Or perhaps you own a pair that can use a boost in the bass or treble departments. Maybe you have excellent earphones, but aren't taking the proper steps to clean them. Whatever the case, you've come to the right place. These basic tips will help you get the most out of your headphones, ensuring they sound their best and remain in tip-top shape for years to come.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How to Care for Your Headphones",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Before we talk about ways to upgrade your audio and user experience, let’s go over some basics to ensure your audio investments have a long, happy life.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Caring for Headphones With Cables",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Now that so many headphones are either wireless or have detachable cables, worrying about wadded-up cabling is becoming a thing of the past. Many included, detachable cables are also just higher quality now than they used to be. There are still hardwired headphones and earphones on the market, though, especially if you're shopping in the audiophile realm."
                      "\n\nIf you have a pair of headphones or in-ears with a cable that can’t be detached by simply unplugging it at the jack, chances are you’re already taking good care of it, but a simple reminder: Cables should be wound in relatively loose loops that follow that natural coil of the wires on the interior. When you wind a cable too tightly or too loosely, it will likely resist by springing out of shape or twisting and resisting your efforts. Try to find the natural coil by winding loops that stay in place—a fastener of some sort will probably be necessary to keep the loop intact."
                      "\n\nFor headphones with detachable cables, the same advice applies, and often, included storage cases will have a compartment designed to hold the looped cable in place. Never wad a cable up and it should last you a very long time.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles5_1.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Caring for Wireless Headphones",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "The good news is, whether you use standard Bluetooth headphones or true wireless earphones, you have a lot less to worry about than anyone still using a wire. It’s true that even true wireless models ship with charging cables, but keeping those cables in good shape (or getting replacements) is easy enough that discussing them much here seems unnecessary."
                      "\n\nParticularly for true wireless in-ears, the most important aspect of maintenance will be protecting the inside of the charging case—mainly from water, but also from earwax. As a rule, you’ll want to make sure your earpieces are thoroughly clean and dry before docking them in a charging case. Any moisture that gets inside the case is going to spell trouble.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "How to Clean Your Headphones",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Cleaning your headphones is another important step in making sure they stay in good working order for the long haul.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Cleaning Headphones",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you own headphones, regardless of whether they're supra-aural (on-ear) or circumaural (over-the-ear), earwax buildup shouldn't be an issue. If the earpads get sweaty (from exercise or just regular use), you might want to rinse them off, but it's very important to first make sure you know the IP rating, which refers in part to the level of water resistance. You’ll want to make sure the rating is at least IPX5, and ideally more like IPX7, if you plan to get them wet or rinse them off."
                      "Even if you can’t get the headphones themselves wet, many earpads are now removable—some can be washed like clothes, others can be run under a faucet and dried, and plenty can be replaced altogether if they get too worn out. But some pads are glued in and have no IP rating, making cleaning them a delicate task—you’ll want to consult the manual for your particular headphones for best practices there."
                      "Likewise, for leather earpads, you should consult the manufacturer’s site or manual for tips on cleaning, as different types of leather may require different approaches. That said, many, if not most earpads are imitation leather, and plenty others are lined with velour- or silk-like fabrics instead. Cleaning these can be as simple as using a slightly damp, lint-free cloth.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles5_2.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Cleaning Earphones",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Whether you have earbuds or in-canal earphones, and whether they are wired, wireless, or completely wire-free, cleaning them can be a little gross, but it’s necessary. Particularly with in-canal models, you'll want to clean them regularly due to the possibility of earwax buildup. Earwax can block out treble, alter the stereo image, and also look mighty unappealing. The good news is you can purchase cleaning tools online for less than \$10. Shure's support site has a helpful guide and video on how to use these cleaning tools, especially with the company's own earphones.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you don’t want to buy an earwax cleaning tool, most eartips can be easily removed from the earpiece, and then run under water to rinse off. You’ll need to dry them thoroughly before putting them back on, and you’ll want to use a soft, lint-free cloth to do so.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Use Apps for Better Sound",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "It's very easy to quickly and radically alter the sound signature of your headphones with an app. Keep in mind, if you don't care for the way they sound out of the box, using an app to tweak things is unlikely to make you suddenly love them. But subtle use of EQ can achieve all kinds of useful results. It's a great way to tone down overly boosted bass (a common feature in today's models) or to tame some overly sibilant high-mids, for instance."
                      "\n\nMany headphones work with dedicated companion apps these days, and there also are plenty of third-party multi-band EQ apps, such as Equalizer+ HD. The trick to these types of apps is to use as little boosting or cutting as possible."
                      "\n\nIf the only thing you really want to alter is the bass response, try boosting it a little and don't fiddle with the other bands. If you want to decrease sibilance on vocals, start by boosting to make them sound more sibilant, then cut the band that increases the sibilance most dramatically—typically somewhere in the middle of the 2kHz-10kHz."
                      "\nnMany of these EQ apps have presets for jazz, rock, or various genres, but customizing your own and using subtle strokes will provide a more rewarding experience that's better tailored to your headphones and your preferences.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles5_3.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Equalizer+ HD offers a number of ways to tweak the sound signature of your headphones",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Beyond EQ settings, some apps offer ways to control or adjust ANC (active noise cancellation) settings, or even blend ANC with ambient listening modes that allow you to hear your surroundings. Still more apps offer control panel customization, which can be as simple as assigning a shortcut function to an on-ear button, or as involved as completely reassigning the function of every swipe, tap, or hold. The app may also allow you to add a voice assistant to your control panel, such as Amazon Alexa or Google Assistant. (Most headphones will default to summoning your mobile device’s built-in assistant, like Apple's Siri.)"
                      "\n\nNearly all of the headphone apps provided by a manufacturer to work directly with a specific pair of headphones will also deliver firmware updates to fix bugs or ensure compatibility with future phone updates. It’s a good idea to check the app for updates on a regular basis.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Up Your Hi-Fi Game With Preamps and DACs",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you have cabled headphones—especially if you have nice ones—you might want to consider kicking things up a notch with a headphone preamp or a DAC. (This can also apply to high quality in-ear models.) Where EQ apps aim to sculpt the sound signature of your headphones' drivers, digital-to-analog converters (DACs) are about improving the overall audio quality, as opposed to the frequency response of the headphones.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles5_4.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Solutions offered by manufacturers range from portable headphone preamp/DACs that plug into your phone, to simple, small DACs for computers and other home sound sources (like the Audioengine D1) that can also be thrown in a bag for portable use."
                      "You plug your headphones into the preamp/DAC, and the results on high-quality headphones are almost always obvious and positive. Most DACs offer higher signal-to-noise ratios, lower distortion, and the ability to play high-resolution files with high bitrate and sample rates without lowering their quality. You don't even have to know what every single one of these features means—the point is, these devices handle a process that most mobile devices dumb down, and they increase the fidelity of your audio.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Start at the (Sound) Source",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Many people use streaming services to listen to audio these days, and some now offer lossless (or relatively lossless) audio streams. But if you care about getting the best audio performance out of your headphones possible, it's worth maintaining a library of high-quality, lossless files for critical/reference/high-fidelity listening scenarios, and saving streaming services for convenience."
                      "\n\nOf course, many streaming services like Spotify perform irreplaceable roles in your life by introducing you to new music. But once you know you really like that new music, it's a good idea to download a high-quality version of the song or album you want to listen to. When you have the option, opt for lossless file formats, like Apple Lossless, FLAC, or pure, uncompressed 24-bit WAV (which is the largest file type, so be aware of storage)."
                      "\n\nIf that isn't an option, make sure you're getting the most you can out of your streams. Go into your service's settings and make sure both Stream Quality and Download Quality are set to the High or Very High. They'll use more bandwidth and storage, but the sound quality will be much better. Spotify's Very High stream quality is 320kbps, which means it has twice as much audio information as a Normal 160kbps stream. Tidal is one of the best-known streaming services dedicated to lossless (1,411Kbps FLAC) music—you pay extra for this Hi-Fi stream, while a regular stream matches Spotify’s Very High quality."
                      "\n\nMost people assume they can't hear the difference between a high-quality, lossless audio file and a low-to-medium-quality stream of the same file. Some listeners will hear the difference immediately, and others might not really notice it at first, but that's only because your ears need a little training. I promise that if you download the highest quality version of a well-recorded, well-mastered track and listen to the 24-bit WAV version exclusively several times over the course of a week, when you listen to a low-bitrate stream of the same track through the same setup and headphones, you will notice differences. Bass response might seem muddier, or less powerful in the sub-bass realm. High-frequency clarity might take a dip, and vocals may sound less crisp. Dynamic range can also suffer. All of which is to say, your source material matters.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bonus Tip: Buy Good Headphones",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Now you know how to properly take care of your headphones and make sure they sound their best. But nothing is more important than making sure you have a good pair to start with."
                      "\n\nFor buying advice, check out our picks for the best headphones, earphones, and true wireless earbuds we've tested.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
