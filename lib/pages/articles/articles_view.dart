import 'package:bluetooth_finder/pages/articles/articles1_page.dart';
import 'package:bluetooth_finder/pages/articles/articles3_page.dart';
import 'package:bluetooth_finder/pages/articles/articles4_page.dart';
import 'package:bluetooth_finder/pages/articles/articles5_page.dart';
import 'package:bluetooth_finder/pages/articles/articles6_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controllers/navigation_bar_controller.dart';
import 'articles10_page.dart';
import 'articles11_page.dart';
import 'articles12_page.dart';
import 'articles13_page.dart';
import 'articles14_page.dart';
import 'articles2_page.dart';
import 'articles7_page.dart';
import 'articles8_page.dart';
import 'articles9_page.dart';

class ArticlesView extends StatelessWidget {
  ArticlesView({Key? key}) : super(key: key);
  final bottomBarController = Get.put(NavigationBarController());
  @override
  Widget build(BuildContext context) {
    print(bottomBarController.articlesPage.value);
    if (bottomBarController.articlesPage.value == 1) return Articles11Page();
    if (bottomBarController.articlesPage.value == 2) return Articles12Page();
    if (bottomBarController.articlesPage.value == 3) return Articles13Page();
    if (bottomBarController.articlesPage.value == 4) return Articles14Page();
    if (bottomBarController.articlesPage.value == 5) return Articles10Page();
    if (bottomBarController.articlesPage.value == 6) return Articles9Page();
    if (bottomBarController.articlesPage.value == 7) return Articles8Page();
    if (bottomBarController.articlesPage.value == 8) return Articles7Page();
    if (bottomBarController.articlesPage.value == 9) return Articles1Page();
    if (bottomBarController.articlesPage.value == 10) return Articles2Page();
    if (bottomBarController.articlesPage.value == 11) return Articles3Page();
    if (bottomBarController.articlesPage.value == 12) return Articles4Page();
    if (bottomBarController.articlesPage.value == 13) return Articles5Page();
    if (bottomBarController.articlesPage.value == 14) return Articles6Page();
    return Container();
  }
}
