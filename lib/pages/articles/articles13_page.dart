import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../controllers/navigation_bar_controller.dart';
import '../../shared/header.dart';

class Articles13Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "5 Things To Check When Buying A Bluetooth Tracker",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://chipolo.net/en/blogs/5-things-to-check-when-buying-a-bluetooth-tracker",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://chipolo.net/en/blogs/5-things-to-check-when-buying-a-bluetooth-tracker"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "BLE trackers use BLE or Bluetooth Low Energy technology and they all connect to your smartphone to keep track of the location of your items, so any one of them will help you find your things. They can all also help you find your phone by making it ring.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "When you think about it, Bluetooth trackers are an amazing piece of innovative tech. They use very little power and harness the smart part of your smartphone to keep track of your things, all at an affordable price."
                      "\n\nBut there are some differences to pay attention to when buying a Bluetooth tracker, and we’re not just talking about the obvious ones, like shape and color.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "1. Battery",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bluetooth trackers have a battery that can last between 6 months to 2 years and in most cases, the batteries are either rechargeable or replaceable or offer a discounted renewal program for the whole tracker when the battery is non-replaceable ."
                      "\n\nBluetooth trackers with rechargeable batteries typically have the shortest battery life and need to be recharged anywhere from every week to every 3 months. In most cases, you can recharge them using a standard USB mini charger (the same you'd use for older smartphones)."
                      "\n\nOnes with replaceable batteries can last anywhere between 6 months to 2 years depending on the model. Once the battery runs out, you can buy a new battery and replace it yourself. The replaceable batteries are usually one of the coin-shaped ones, which you can find in most shops and online. They will cost between 1 – 5 \$, depending on the brand."
                      "\n\nSome smart trackers require a specialized tool to open the tracker and replace the battery, which most tracker manufacturers will include in your order."
                      "\n\nTrackers with non-replaceable batteries will last longer, usually up to 1 year."
                      "\n\nSince you have to replace the whole tracker when the battery runs out, most companies offer a discounted repurchase program, where you can get the new tracker at a discounted price of up to 50%, and at the same time, they will also help you recycle the old one."
                      "\n\nThe costs of replacing a tracker every year are a bit higher than just replacing the battery, but the good news is these discounts usually apply to any tracker the company offers, so you can upgrade your tracker to the newest model or just go with a different color to change it up.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "2. Range",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Any technology based on Bluetooth will work optimally at a range of up to 30 feet. With Bluetooth or BLE trackers this range is extended to about 100 feet and a handful of companies have further stretched this so that your phone can stay connected to your tracker even at distances of 200 feet."
                      "\n\nUltimately the connection range will depend on your surroundings."
                      "\n\nObstacles like thick walls, trees, and cars can decrease the connectivity range."
                      "\n\nCompanies will usually list the maximum range of a tracker in their product descriptions, so make sure to check out a few independent reviews before buying."
                      "\n\nCheck how those numbers hold up in an actual test. We recommend the YouTube reviewer ModernDayFamilyMan, as he likes to test any claims Bluetooth tracker companies make regarding their products.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "3. Sound",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Bluetooth finders are made to be heard, so one of the most important features is how loud the Bluetooth tracker is."
                      "\n\nThe tracker’s ringing must be loud enough to be heard even if your lost item is buried under a couch cushion, a pile of toys, or if you find yourself searching in a noisy environment."
                      "\n\nPay attention to the ringer melody too. Will you be able to hear it in a noisy environment? Does the company offer a few different ringtones you can choose from or do they have one default ringer for all of their devices?",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "4. Use case",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Think about what you’ll be using the Bluetooth tracker for before your purchase. Will you use it to find your keys, your wallet, your backpack, or to track your pet?"
                      "\n\nSome Bluetooth trackers have additional features like a thin, card-like shape for your wallet, water-resistance or out of range alerts on your phone when the tracker goes out of range."
                      "\n\nSome you can share with your family and friends through the accompanying app if the item you want to track is used by a few people.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "5. Price",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Like with every other product, the prices of Bluetooth trackers can go from cheap to incredibly expensive."
                      "\n\nYou can factor the tracker’s accompanying features into how much you’re willing to spend, but there’s another great way to check if a tracker’s price is worth it before you buy."
                      "\n\nYou can view the accompanying app ratings and user feedback to check how satisfied others are with their product and how helpful the company is if something goes wrong.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
