import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../config/colors.dart';
import '../../controllers/navigation_bar_controller.dart';
import '../../shared/block_faq.dart';
import '../../shared/header.dart';

class Articles2Page extends StatelessWidget {
  Articles2Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.articlesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If your Apple Watch is lost or stolen",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Source https://support.apple.com/en-us/HT207024",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w400,
                          fontSize: 13,
                          color: Theme.of(context).hoverColor.withOpacity(0.4)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () {
                      final browser = ChromeSafariBrowser();
                      browser.open(
                          url: Uri.parse(
                              "https://support.apple.com/en-us/HT207024"),
                          options: ChromeSafariBrowserClassOptions(
                              android: AndroidChromeCustomTabsOptions(
                                  addDefaultShareMenuItem: false,
                                  keepAliveEnabled: true),
                              ios: IOSSafariOptions(
                                  dismissButtonStyle:
                                      IOSSafariDismissButtonStyle.CLOSE,
                                  presentationStyle: IOSUIModalPresentationStyle
                                      .OVER_FULL_SCREEN)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Text(
                        "Read on site..",
                        style: GoogleFonts.manrope(
                            height: 1.6,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Color.fromRGBO(0, 122, 255, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If your Apple Watch is missing, the Find My app can help you find it and protect your information.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Here's how it works",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Find My can help you locate and protect your missing Apple Watch. If you already set up Find My iPhone on your paired iPhone, it's automatically enabled on your Apple Watch and any Apple Watch paired using Family Setup. So if your watch is lost or stolen, you can use Find My to help you find it again. And thanks to Activation Lock, your Apple ID and password are required before anyone can erase your Apple Watch and use it with their iPhone.\n\nWhen you use Find My, your Apple Watch with GPS and cellular can use GPS and a trusted Wi-Fi or cellular connection to show you its approximate location. Apple Watch with GPS can use GPS and a trusted Wi-Fi connection. Since Apple Watch Series 1 doesn't have GPS, you'll see the location of your paired iPhone or its Wi-Fi connection.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24), child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "See your watch on a map",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 24, right: 28),
                    child: Text(
                      "Sign in at iCloud.com or use the Find My app to see your watch on a map. If your watch is nearby, you can tap Play Sound to help you find it. Your watch rings until you tap Dismiss.\n\nIf you don't see your watch on the map, then it might not be connected to Wi-Fi, cellular, or your paired iPhone.\n\nOn your computer" +
                          "\n   1.  Go to iCloud.com, then sign in with your Apple ID." +
                          "\n   2. Open Find iPhone." +
                          "\n   3. Click All Devices, then click your Apple Watch." +
                          "\n\nOn your iPhone" +
                          "\n   1.  Open the Find My app." +
                          "\n   2. Choose the Devices tab." +
                          "\n   3. Select your Apple Watch to see its location on the map.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles2_1.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Put your Apple Watch in Lost Mode",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If your Apple Watch goes missing, you can immediately lock it from iCloud.com or your paired iPhone. Just place your watch in Lost Mode. Then you can send it a custom message with your phone number. So if someone finds your Apple Watch, they know how to contact you." +
                          "\n\nOn your iPhone, iPad, or iPod touch" +
                          "\n   1.  Open Find My and tap your Apple Watch."
                              "\n   2. Tap Activate in the Mark As Lost section."
                              "\n   3. Tap Continue."
                              "\n   4. Enter a phone number where you can be reached, then tap Next."
                              "\n   5. Enter a message that you want to show on the watch screen."
                              "\n   6. Tap Activate."
                              "\n\nFind My sends you an email message to confirm that you put your Apple Watch in Lost Mode.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 24, right: 24),
                      child: Image.asset(
                        "assets/images/articles2_2.png",
                      )),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "Turn off or cancel Lost Mode",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you find your missing Apple Watch, tap Unlock on your watch, then enter your passcode. Lost Mode will automatically turn off, and you can start using your watch again as normal. You can also turn off Lost Mode from your paired iPhone or iCloud.com."
                      "\n\nOn your computer"
                      "\n   1.  Sign in to iCloud.com with your Apple ID."
                      "\n   2. Click Find iPhone."
                      "\n   3. Click All Devices, then click your Apple Watch."
                      "\n   4. Click Lost Mode > Stop Lost Mode, then click Stop Lost Mode again to confirm."
                      "\n\nOn your iPhone"
                      "\n   1.  Open the Find My app and tap your missing Apple Watch."
                      "\n   2. Tap Activated in the Mark As Lost section."
                      "\n   3. Tap Turn Off Mark As Lost, then tap Turn Off to confirm.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        left: 24,
                      ),
                      child: Divider()),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you don't see your Apple Watch on the map",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      "If you didn't turn on Find My before your Apple Watch was lost or stolen and your watch isn't connected to Wi-Fi, cellular, or your paired iPhone, you can't use it to locate your device. However, you can use these steps to help protect your information:"
                      "\n\n   1.  Place your Apple Watch in Lost Mode. When your watch is in Lost Mode, your passcode is required before anyone can turn off Find My, erase your watch, or pair it with another iPhone."
                      "\n   2. Change your Apple ID password. When you change your Apple ID password, you can prevent anyone from accessing your iCloud information or from using other services from your missing device."
                      "\n   3. Report your lost or stolen device to local law enforcement. Law enforcement might request the serial number of your device. Find your device serial number."
                      "\n\nFind My is the only way that you can track or locate a lost or missing device. If Find My isn't enabled on your watch before it goes missing, there is no other Apple service that can find, track, or otherwise flag your device for you.",
                      style: GoogleFonts.manrope(
                          height: 1.6,
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  SizedBox(
                    height: 26,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
