import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../config/colors.dart';
import '../config/text.dart';
import '../controllers/navigation_bar_controller.dart';
import '../shared/block_faq.dart';
import '../shared/header.dart';

class SupportPage extends StatefulWidget {
  @override
  State<SupportPage> createState() => _SupportPageState();
}

class _SupportPageState extends State<SupportPage> {
  String? selectedValue;
  String? text;

  List<DropdownMenuItem<String>> _addDividersAfterItems(List<String> items) {
    List<DropdownMenuItem<String>> _menuItems = [];
    for (var item in items) {
      _menuItems.addAll(
        [
          DropdownMenuItem<String>(
            value: item,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                item,
                style: GoogleFonts.manrope(
                  fontSize: 17.sp,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(37, 37, 37, 1),
                ),
              ),
            ),
          ),
          //If it's last item, we will not add Divider after it.
          if (item != items.last)
            const DropdownMenuItem<String>(
              enabled: false,
              child: Divider(),
            ),
        ],
      );
    }
    return _menuItems;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Support",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.settingsPage.value = 0;
                        settingsController.devicesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 24.h,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "If the device is not found, please leave information about your device",
                    style: GoogleFonts.manrope(
                        fontWeight: FontWeight.w600,
                        fontSize: 17.sp,
                        color: Theme.of(context).hoverColor),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Text(
                    "Device type",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.manrope(
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(70, 70, 70, 1),
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                    width: MediaQuery.of(context).size.width - 48,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton2(
                        isExpanded: true,
                        icon: Image.asset(
                          "assets/images/arrow_down.png",
                          width: 24,
                        ),
                        hint: Text(
                          'Device type',
                          style: GoogleFonts.manrope(
                            fontSize: 17.sp,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(209, 209, 209, 1),
                          ),
                        ),
                        items: _addDividersAfterItems(TextConfig.devices),
                        value: selectedValue,
                        onChanged: (value) {
                          setState(() {
                            selectedValue = value as String;
                          });
                        },
                        dropdownDecoration: BoxDecoration(
                          boxShadow: [BoxShadow()],
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(8),
                            bottomRight: Radius.circular(8),
                          ),
                          color: Color.fromRGBO(245, 245, 245, 1),
                        ),
                        offset: const Offset(-16, 0),
                        dropdownWidth: MediaQuery.of(context).size.width - 48,
                        itemPadding: EdgeInsets.only(left: 16, right: 16),
                        buttonHeight: 40.h,
                        buttonWidth: 140.w,
                        itemHeight: 25.h,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Text(
                    "Brand, model",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.manrope(
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(70, 70, 70, 1),
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Color.fromRGBO(245, 245, 245, 1),
                      ),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 12.h),
                      child: TextField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Brand, model",
                            hintStyle: GoogleFonts.manrope(
                                color: Color.fromRGBO(209, 209, 209, 1))),
                        onChanged: (inp) {
                          setState(() {
                            text = inp;
                          });
                        },
                      )),
                  Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: InkWell(
                        focusColor: Color.fromRGBO(0, 0, 0, 0),
                        hoverColor: Color.fromRGBO(0, 0, 0, 0),
                        splashColor: Color.fromRGBO(0, 0, 0, 0),
                        highlightColor: Color.fromRGBO(0, 0, 0, 0),
                        onTap: () {
                          print(text);
                          final settingsController =
                              Get.put(NavigationBarController());
                          settingsController.settingsPage.value = 0;
                          settingsController.devicesPage.value = 0;
                          settingsController.update();
                        },
                        child: Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 24),
                              height: 56,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color: Colors.white),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 24),
                              height: 56,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color: (text ?? "").isEmpty
                                      ? Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.6)
                                      : Theme.of(context).focusColor),
                              child: Center(
                                child: Text(
                                  "SEND",
                                  style: GoogleFonts.manrope(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 17,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
