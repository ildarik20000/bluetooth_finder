import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../config/colors.dart';
import '../controllers/navigation_bar_controller.dart';
import '../shared/block_faq.dart';
import '../shared/header.dart';

class ArticlesPage extends StatelessWidget {
  ArticlesPage({Key? key}) : super(key: key);
  final settingsController = Get.put(NavigationBarController());
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Articles",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  // Align(
                  //   alignment: Alignment.centerLeft,
                  //   child: InkWell(
                  //     onTap: () {
                  //       final settingsController =
                  //           Get.put(NavigationBarController());
                  //       settingsController.ar.value = 0;
                  //       settingsController.update();
                  //     },
                  //     child: Container(
                  //       child: Image.asset(
                  //         "assets/images/arrow_left.png",
                  //         color: Theme.of(context).hoverColor,
                  //         width: 24,
                  //       ),
                  //     ),
                  //   ),
                  // )
                ],
              ),
              context),
          SizedBox(
            height: 8,
          ),
          Expanded(
            child: ListView.builder(
                itemCount: settingsController.articlesTitle.length,
                itemBuilder: (context, index) {
                  return block(settingsController.articlesTitle[index], () {
                    settingsController.articlesPage.value = index + 1;
                    settingsController.update();
                  }, context);
                }),
          ),
        ],
      ),
    );
  }

  Widget block(String text, VoidCallback onTap, BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 16,
        left: 24,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            hoverColor: Color.fromRGBO(0, 0, 0, 0),
            splashColor: Color.fromRGBO(0, 0, 0, 0),
            focusColor: Color.fromRGBO(0, 0, 0, 0),
            highlightColor: Color.fromRGBO(0, 0, 0, 0),
            onTap: onTap,
            child: Container(
              padding: EdgeInsets.only(right: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      text,
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 17,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Image.asset(
                    "assets/images/arrow_right2.png",
                    width: 24,
                    color: Theme.of(context).hoverColor,
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Divider(
            height: 2,
          ),
        ],
      ),
    );
  }
}
