import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:bluetooth_finder/shared/block_faq_special.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../config/colors.dart';
import '../controllers/navigation_bar_controller.dart';
import '../shared/block_faq.dart';
import '../shared/header.dart';

class FAQPage extends StatefulWidget {
  FAQPage({Key? key}) : super(key: key);

  @override
  State<FAQPage> createState() => _FAQPageState();
}

class _FAQPageState extends State<FAQPage> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "FAQ",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.settingsPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 8,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BlockFAQ("How does it work?",
                      "Our application shows the distance to Bluetooth devices based on the strength of the signal that comes from them. The stronger the signal, the closer the device and by its strength we can roughly estimate the distance to the device in meters."),
                  BlockFAQ(
                      "Is it possible to know not only the distance, but also the direction of the device?",
                      "By signal strength, we can only tell how far the device is and it will be in this radius from you. However, if you try to move in different directions, you will see that when moving in one direction, the distance decreases, which means that your device is in that direction."),
                  BlockFAQ("How accurate is the distance shown?",
                      "We cannot guarantee that the device is really exactly N meters away from you, there are always various kinds of errors, for example, something can interfere with the signal and our application will determine the distance at 20 meters, although in fact the device is at a distance of 10 and something is interfering with the signal. \n\nHowever, we try our best to increase the accuracy and according to our internal tests, the error in distance under normal conditions is at the level of 5-10%."),
                  BlockFAQSpecial(
                      "I can't see my device, what should I do?",
                      "• Try restarting the app or just refresh the list of found devices. This is unlikely, but it may not have picked up a signal from your device.\n• Restart Bluetooth on your phone, it may not have received a signal from your device\n• Move in different directions, the device may be too far away and the signal from it does not reach your phone\n• If the problem persists, please write to us with a",
                      " special form",
                      " and we will try to solve this problem."),
                  BlockFAQ("Can I find my Wi-Fi  device?",
                      "At the moment, no, however we are already working on an update to add this feature."),
                  BlockFAQ(
                      "Does the device need to be paired with my phone to be found?",
                      "No it shouldn't, theoretically you can find any device that has Bluettoth enabled."),
                  BlockFAQ(
                      "Can I find my headphones using the app if they are in a case?",
                      "No, the headphones do not use Bluetooth in the case, so it will not be possible to detect them in this way.")
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
