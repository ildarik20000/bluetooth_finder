import 'package:bluetooth_finder/controllers/theme_controller.dart';
import 'package:bluetooth_finder/main.dart';
import 'package:bluetooth_finder/pages/subscription_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';

import '../config/colors.dart';
import '../controllers/navigation_bar_controller.dart';
import '../shared/header.dart';

class ThemePage extends StatelessWidget {
  ThemePage({Key? key}) : super(key: key);
  final themeDataController = Get.put(ThemeDataController());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Theme",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.settingsPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 32,
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Color.fromRGBO(246, 246, 246, 1)),
            padding: EdgeInsets.all(19),
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              children: [
                themeButton("Classic", context,
                    Hive.box('settings').get('theme', defaultValue: 0) == 0,
                    () {
                  themeDataController.setTheme(0);
                }),
                Divider(
                  height: 40,
                ),
                themeButton("Red", context,
                    Hive.box('settings').get('theme', defaultValue: 0) == 1,
                    () {
                  if (subscribe)
                    themeDataController.setTheme(1);
                  else {
                    Get.to(SubscriptionPage());
                  }
                }),
                Divider(
                  height: 40,
                ),
                themeButton("Green", context,
                    Hive.box('settings').get('theme', defaultValue: 0) == 2,
                    () {
                  if (subscribe)
                    themeDataController.setTheme(2);
                  else {
                    Get.to(SubscriptionPage(
                      
                    ));
                  }
                }),
                Divider(
                  height: 40,
                ),
                themeButton("Dark 1", context,
                    Hive.box('settings').get('theme', defaultValue: 0) == 3,
                    () {
                  if (subscribe)
                    themeDataController.setTheme(3);
                  else {
                    Get.to(SubscriptionPage());
                  }
                }),
                Divider(
                  height: 40,
                ),
                themeButton("Dark 2", context,
                    Hive.box('settings').get('theme', defaultValue: 0) == 4,
                    () {
                  if (subscribe)
                    themeDataController.setTheme(4);
                  else {
                    Get.to(SubscriptionPage());
                  }
                }),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget themeButton(
      String text, BuildContext context, bool select, VoidCallback onChange) {
    return InkWell(
      onTap: () {
        onChange();
      },
      child: Container(
        child: Row(children: [
          Text(text,
              style: GoogleFonts.manrope(
                fontWeight: FontWeight.w500,
                fontSize: 20,
                color: ColorsConfig.black,
              )),
          Expanded(
            child: Align(
                alignment: Alignment.centerRight,
                child: Container(
                  width: 24,
                  height: 24,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Transform.scale(
                    scale: 1.3,
                    child: Checkbox(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      visualDensity:
                          VisualDensity(horizontal: 1, vertical: 0.4),
                      side: BorderSide(color: Colors.black, width: 2),
                      checkColor: Colors.white,
                      focusColor: Theme.of(context).focusColor,
                      hoverColor: Theme.of(context).focusColor,
                      activeColor: Theme.of(context).focusColor,
                      onChanged: (bool? value) {
                        onChange();
                      },
                      value: select,
                    ),
                  ),
                )),
          ),
        ]),
      ),
    );
  }
}
