import 'package:bluetooth_finder/config/colors.dart';
import 'package:bluetooth_finder/config/text.dart';
import 'package:bluetooth_finder/controllers/navigation_bar_controller.dart';
import 'package:bluetooth_finder/main.dart';
import 'package:bluetooth_finder/models/gadgets_model.dart';
import 'package:bluetooth_finder/pages/subscription_page.dart';
import 'package:bluetooth_finder/services/find_bluetooth.dart';
import 'package:bluetooth_finder/shared/devices.dart';
import 'package:bluetooth_finder/shared/header.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';

import '../controllers/find_gadgets_controller.dart';
import '../shared/map_devices.dart';

class MapPage extends StatelessWidget {
  final gadgetsController = Get.put(FindGadgetsController());
  final bottomBarNavigation = Get.put(NavigationBarController());
  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      headerBlock(
          Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Find Gadgets",
                  style: GoogleFonts.manrope(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                      color: Theme.of(context).hoverColor),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: InkWell(
                  onTap: () {
                    bottomBarNavigation.devicesPage.value = 0;
                    bottomBarNavigation.update();
                  },
                  child: Container(
                    child: Image.asset(
                      "assets/images/back_device.png",
                      color: Theme.of(context).hoverColor,
                      width: 24,
                    ),
                  ),
                ),
              )
            ],
          ),
          context),
      Expanded(
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                "assets/images/background.png",
                fit: BoxFit.fill,
                color: Theme.of(context).focusColor.withOpacity(0.3),
              ),
            ),
            GetBuilder<FindGadgetsController>(builder: (_) {
              return ListView.builder(
                  padding:
                      EdgeInsets.only(bottom: 50.h, left: 20.w, right: 20.w),
                  reverse: true,
                  itemCount: gadgetsController.sortGadgets.length,
                  itemBuilder: (context, index) {
                    return Align(
                      alignment: index.isEven
                          ? Alignment.centerLeft
                          : Alignment.centerRight,
                      child: InkWell(
                        onTap: () {
                          if (subscribe ||
                              Hive.box('settings')
                                      .get('tapDevice', defaultValue: 0) <
                                  3) {
                            Hive.box('settings').put(
                                'tapDevice',
                                Hive.box('settings')
                                        .get('tapDevice', defaultValue: 0) +
                                    1);
                            final devicesController =
                                Get.put(FindGadgetsController());
                            final bottomBarController =
                                Get.put(NavigationBarController());
                            devicesController.devicesTap.value =
                                gadgetsController.sortGadgets[index].id;
                            bottomBarController.devicesPage.value = 2;
                            gadgetsController.devicesTapName.value =
                                gadgetsController.sortGadgets[index].name;
                            bottomBarController.update();
                            devicesController.update();
                          } else {
                            Get.to(SubscriptionPage(
                              fromDevice: true,
                            ));
                          }
                        },
                        child: Container(
                            width: MediaQuery.of(context).size.width / 2,
                            constraints: BoxConstraints(
                              minWidth: 35.0,
                              maxWidth: MediaQuery.of(context).size.width,
                            ),
                            padding: EdgeInsets.only(top: 40.h),
                            child: MapDevices(
                                gadgetsController.sortGadgets[index].name,
                                gadgetsController.sortGadgets[index].distance,
                                gadgetsController
                                    .sortGadgets[index].type.name)),
                      ),
                    );
                  });
            })
          ],
        ),
      )
    ]);
  }
}
