import 'package:bluetooth_finder/config/colors.dart';
import 'package:bluetooth_finder/controllers/navigation_bar_controller.dart';
import 'package:bluetooth_finder/main.dart';
import 'package:bluetooth_finder/services/browser_services.dart';
import 'package:bluetooth_finder/shared/header.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'subscription_page.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Settings",
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: sharePressed,
                      child: Container(
                        child: Image.asset(
                          "assets/images/share.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 32,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    buttonSettings("change_theme.png", "Change the theme", () {
                      if (subscribe) {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.settingsPage.value = 1;
                        settingsController.update();
                      } else {
                        Get.to(() => SubscriptionPage(
                          fromDevice: false,
                        ));
                      }
                    }),
                    SizedBox(
                      height: 16,
                    ),
                    buttonSettings("faq.png", "FAQ", () {
                      final settingsController =
                          Get.put(NavigationBarController());
                      settingsController.settingsPage.value = 2;
                      settingsController.update();
                    }),
                    SizedBox(
                      height: 16,
                    ),
                    ttPPSettings(),
                    SizedBox(
                      height: 16,
                    ),
                    buttonSettings("support.png", "Support", () {
                      final settingsController =
                          Get.put(NavigationBarController());
                      settingsController.settingsPage.value = 3;
                      settingsController.update();
                    }),
                    SizedBox(
                      height: 16,
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buttonSettings(String image, String text, VoidCallback onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Color.fromRGBO(246, 246, 246, 1)),
        padding: EdgeInsets.all(19),
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Image.asset(
            "assets/images/" + image,
            width: 30,
          ),
          SizedBox(
            width: 20,
          ),
          Text(text,
              style: GoogleFonts.manrope(
                fontWeight: FontWeight.w500,
                fontSize: 20,
                color: ColorsConfig.black,
              )),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Image.asset(
                "assets/images/arrow_right.png",
                width: 9,
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Widget ttPPSettings() {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Color.fromRGBO(246, 246, 246, 1)),
      padding: EdgeInsets.all(19),
      child: Column(
        children: [
          InkWell(
            onTap: termsOfUsePressed,
            child:
                Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
              Image.asset(
                "assets/images/term_of_use.png",
                width: 30,
              ),
              SizedBox(
                width: 20,
              ),
              Text("Term of use",
                  style: GoogleFonts.manrope(
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                    color: ColorsConfig.black,
                  )),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Image.asset(
                    "assets/images/arrow_right.png",
                    width: 9,
                  ),
                ),
              ),
            ]),
          ),
          Divider(
            height: 40,
          ),
          InkWell(
            onTap: privacyPolicyPressed,
            child:
                Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
              Image.asset(
                "assets/images/privacy_policy.png",
                width: 30,
              ),
              SizedBox(
                width: 20,
              ),
              Text("Privacy policy",
                  style: GoogleFonts.manrope(
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                    color: ColorsConfig.black,
                  )),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Image.asset(
                    "assets/images/arrow_right.png",
                    width: 9,
                  ),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
