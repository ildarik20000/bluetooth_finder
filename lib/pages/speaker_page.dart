import 'package:bluetooth_finder/controllers/find_gadgets_controller.dart';
import 'package:bluetooth_finder/shared/header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/navigation_bar_controller.dart';

class SpeakerPage extends StatelessWidget {
  
  final devicesController = Get.put(FindGadgetsController());
  SpeakerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          headerBlock(
              Stack(
                children: [
                  Container(),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      devicesController.devicesTapName.value,
                      style: GoogleFonts.manrope(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Theme.of(context).hoverColor),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () {
                        final settingsController =
                            Get.put(NavigationBarController());
                        settingsController.devicesPage.value = 0;
                        settingsController.update();
                      },
                      child: Container(
                        child: Image.asset(
                          "assets/images/arrow_left.png",
                          color: Theme.of(context).hoverColor,
                          width: 24,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              context),
          SizedBox(
            height: 51,
          ),
          GetBuilder<FindGadgetsController>(builder: (_) {
            return Center(
              child: Container(
                height: 343,
                width: 343,
                child: Stack(
                  children: [
                    Center(
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        height: 150 +
                            double.parse(devicesController
                                    .devicesTapDistance.value) *
                                1.99,
                        width: 150 +
                            double.parse(devicesController
                                    .devicesTapDistance.value) *
                                1.99,
                        decoration: BoxDecoration(
                          gradient: RadialGradient(
                            colors: [
                              Theme.of(context).indicatorColor,
                              Theme.of(context).focusColor,
                            ],
                          ),
                          borderRadius: BorderRadius.circular(200),
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        height: 343,
                        width: 343,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(200),
                            border: Border.all(
                                width: 1,
                                color: Theme.of(context).splashColor)),
                        child: Center(
                          child: Container(
                            height: 278,
                            width: 278,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(200),
                                border: Border.all(
                                    width: 1,
                                    color: Theme.of(context).splashColor)),
                            child: Center(
                              child: Container(
                                height: 214,
                                width: 214,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(200),
                                    border: Border.all(
                                        width: 1,
                                        color: Theme.of(context).splashColor)),
                                child: Center(
                                  child: Container(
                                    height: 150,
                                    width: 150,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(200),
                                        border: Border.all(
                                            width: 1,
                                            color:
                                                Theme.of(context).splashColor)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "~${devicesController.devicesTapDistance}m",
                                          style: GoogleFonts.manrope(
                                              fontWeight: FontWeight.w800,
                                              fontSize: 40,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          "Approximate\ndistance",
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.manrope(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
          SizedBox(
            height: 56,
          ),
          GetBuilder<FindGadgetsController>(builder: (_) {
            return devicesController.devicesTapType.value
                ? InkWell(
                  onTap: () {
                    MethodChannel('ring').invokeMethod('ring');
                  },
                  child: Container(
                      width: 263,
                      height: 56,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Theme.of(context).focusColor),
                      child: Center(
                        child: Text(
                          "Ring",
                          style: GoogleFonts.manrope(
                              fontWeight: FontWeight.w700,
                              fontSize: 20,
                              color: Colors.white),
                        ),
                      ),
                    ),
                )
                : Container();
          }),
          SizedBox(
            height: 16,
          ),
          InkWell(
            onTap: () {
              final settingsController = Get.put(NavigationBarController());
              settingsController.devicesPage.value = 0;
              settingsController.update();
            },
            child: Container(
              width: 263,
              height: 56,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  border: Border.all(color: Theme.of(context).focusColor)),
              child: Center(
                child: Text(
                  "Found",
                  style: GoogleFonts.manrope(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: Theme.of(context).focusColor),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
