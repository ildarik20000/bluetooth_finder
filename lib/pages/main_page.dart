import 'package:bluetooth_finder/main.dart';
import 'package:bluetooth_finder/pages/articles_page.dart';
import 'package:bluetooth_finder/pages/devices_page.dart';
import 'package:bluetooth_finder/pages/faq_page.dart';
import 'package:bluetooth_finder/pages/map_page.dart';
import 'package:bluetooth_finder/pages/settings_page.dart';
import 'package:bluetooth_finder/pages/speaker_page.dart';
import 'package:bluetooth_finder/pages/support_page.dart';
import 'package:bluetooth_finder/pages/theme_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/find_gadgets_controller.dart';
import '../controllers/navigation_bar_controller.dart';
import '../services/find_bluetooth.dart';
import 'articles/articles1_page.dart';
import 'articles/articles2_page.dart';
import 'articles/articles3_page.dart';
import 'articles/articles4_page.dart';
import 'articles/articles5_page.dart';
import 'articles/articles6_page.dart';
import 'articles/articles_view.dart';

class MainPage extends StatefulWidget {
  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (!init) FindBluetooth().initBluetooth();
  }

  final bottomBarController = Get.put(NavigationBarController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NavigationBarController>(builder: (_) {
      return Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          bottomNavigationBar: Container(
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(0, 0, 0, 0.3)))),
            child: BottomNavigationBar(
              // unselectedItemColor: Colors.black,
              // selectedItemColor: Colors.redAccent,
              onTap: bottomBarController.changeTabIndex,
              currentIndex: bottomBarController.tabIndex.value,
              showSelectedLabels: false,
              showUnselectedLabels: false,
              type: BottomNavigationBarType.fixed,
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              elevation: 0,
              items: [
                bottomNavigationBarItem(
                    "assets/images/Settings.png", "Settings", context),
                bottomNavigationBarItem(
                    "assets/images/Devices.png", "Devices", context),
                bottomNavigationBarItem(
                    "assets/images/Articles.png", "Articles", context),
              ],
            ),
          ),
          body: ScreenUtilInit(
            designSize: const Size(375, 844),
            builder: () => SafeArea(
                child: IndexedStack(
              index: bottomBarController.pageIndex,
              children: [
                if (bottomBarController.settingsPage.value == 0) SettingsPage(),
                if (bottomBarController.settingsPage.value == 1) ThemePage(),
                if (bottomBarController.settingsPage.value == 2) FAQPage(),
                if (bottomBarController.settingsPage.value == 3) SupportPage(),
                if (bottomBarController.devicesPage.value == 0) DevicesPage(),
                if (bottomBarController.devicesPage.value == 1) MapPage(),
                if (bottomBarController.devicesPage.value == 2) SpeakerPage(),
                if (bottomBarController.devicesPage.value == 3) SupportPage(),
                if (bottomBarController.articlesPage.value == 0) ArticlesPage(),
                if (bottomBarController.articlesPage.value != 0) ArticlesView(),

                
              ],
            )),
          ));
    });
  }

  BottomNavigationBarItem bottomNavigationBarItem(
      String images, String label, BuildContext context) {
    return BottomNavigationBarItem(
        icon: Column(
          children: [
            Image.asset(images,
                width: 24,
                color: Theme.of(context).focusColor.withOpacity(0.6)),
            Text(
              label,
              style: GoogleFonts.manrope(
                  fontWeight: FontWeight.w500,
                  fontSize: 10,
                  color: Theme.of(context).focusColor.withOpacity(0.6)),
            )
          ],
        ),
        activeIcon: Column(
          children: [
            Image.asset(images, width: 24, color: Theme.of(context).focusColor),
            Text(
              label,
              style: GoogleFonts.manrope(
                  fontWeight: FontWeight.w500,
                  fontSize: 10,
                  color: Theme.of(context).focusColor),
            ),
          ],
        ),
        label: label);
  }
}
