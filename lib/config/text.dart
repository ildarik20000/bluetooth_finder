class TextConfig {
  static final String all = "All";
  static final String pc = "PC";
  static final String laptop = "Laptop";
  static final String tablet = "Tablet";
  static final String speaker = "Speaker";
  static final String mouse = "Mouse";
  static final String headphones = "Headphones";
  static final String smartphone = "Smartphone";
  static final String watch = "Watch";
  static final String other = "Other";
  static final String connectDevices = "All devices";
  static final String availableDevices = "Available devices";
  static final devices = <String>[
    'PC',
    'Laptop',
    'Tablet',
    'Speaker',
    'Mouse',
    'Headphones',
    'Smartphone',
    'Watch',
    'Other'
  ];
}
